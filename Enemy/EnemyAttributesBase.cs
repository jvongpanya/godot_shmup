﻿using Godot;
using Shmupper.Player.Impl;
using Shmupper.scenes.bullets.Impl;
using Shmupper.SharedInterfaces;
using Shmupper.Utilities;

namespace Shmupper.Enemy
{
    /// <summary>
    /// Represents the base attributes of the enemy.
    /// This is an Area2D to represent that it an object with space in the Godot engine
    /// </summary>
    public class EnemyAttributesBase : Area2D, IHasScore
    {
        /// <summary>
        /// These are all signals that are emitted under special conditions
        /// </summary>
        #region Signals
        [Signal]
        public delegate void enemy_destroyed();

        /// <summary>
        /// This is emitted when the enemy is destroyed with the correct bullet
        /// </summary>
        [Signal]
        public delegate void enemy_destroyed_special();

        [Signal]
        public delegate void boss_destroyed();
        #endregion

        /// <summary>
        /// The stats for the enemy
        /// </summary>
        #region Stats
        [Export]
        public float Hp = 20;

        [Export]
        public int Score = 100;

        [Export]
        public int CorrectShotMultiplier = 3;

        /// <summary>
        /// The unique identifier of this enemy 
        /// </summary>
        public string EnemyId { get; set; }

        public int InternalScore { get => Score; set => Score = value; }
        #endregion

        [Export]
        public bool IsAirborne = true;

        [Export]
        public bool IsBoss = false;

        /// <summary>
        /// Anything related to items or external scenes for this enemy
        /// </summary>
        #region Item Drop
        [Export]
        public PackedScene ItemDrop;

        [Export]
        public PackedScene Explosion;

        [Export]
        public bool DropItem = true;
        #endregion

        private GlobalPlayerStats _globalPlayerStats;

        public override void _Ready()
        {
            base._Ready();
            SetupId();
            _globalPlayerStats = StageConstants.GetGlobalPlayerStatsNode(this);
            ((AnimationPlayer)GetNode("AnimationPlayer")).Play("default");

            SetupId();

            var stageRoot = StageConstants.GetStageNode(this);
            this.Connect(nameof(boss_destroyed), stageRoot, "end_stage"); // Connects this entity's boss_destroyed event to the stage's end_stage event

            var playerNode = StageConstants.GetPlayerNode(this);
            if(playerNode != null)
                this.Connect(nameof(boss_destroyed), playerNode, "end_stage"); // Connects this entity's boss_destroyed event to the player's end_stage event
        }

        /// <summary>
        /// Randomly generates an ID for this enemy, which should be unique.
        /// </summary>
        private void SetupId()
        {
            var rng = new RandomNumberGenerator();
            rng.Randomize();
            EnemyId = "enemy" + rng.Randi(); // Use RandomNumberGenerator since it is shorter than GUID
        }

        /// <summary>
        /// Affects this enemy when a player bullet enters this enemy's area
        /// </summary>
        /// <param name="area"></param>
        public void _on_Enemy_area_entered(Area2D area)
        {
            if (area.IsInGroup(GroupConstants.PLAYER_BULLET_GROUP))
            {
                ManageHp(area);
                RemoveBullet(area);

                if (Hp <= 0)
                {
                    _globalPlayerStats.AddToScore(this);

                    // Determines if the correct shot is used based on this enemy's group, and the player bullet's group
                    var correctShot = (IsInGroup(GroupConstants.ENEMY_ANGEL_GROUP) && area.IsInGroup(GroupConstants.PLAYER_ANGEL_BULLET_GROUP)) 
                        || (IsInGroup(GroupConstants.ENEMY_DEMON_GROUP) && area.IsInGroup(GroupConstants.PLAYER_DEMON_BULLET_GROUP));
                    
                    // If the player bullet was special, emit the enemy_destroyed_special signal to trigger any special events.
                    if (area.IsInGroup(GroupConstants.PLAYER_SPECIAL_BULLET))
                    {
                        EmitSignal(nameof(enemy_destroyed_special), EnemyId);
                    }
                    else
                    {
                        // Otherwise, call drop_item(). Must be deferred so the method runs asynchronously.
                        CallDeferred("drop_item", correctShot ? CorrectShotMultiplier : 1);
                    }

                    if (IsBoss)
                        EmitSignal(nameof(boss_destroyed));

                    // Must be deferred so the method runs asynchronously.
                    CallDeferred("spawn_explosion");
                    QueueFree();
                }
            }
        }

        private void ManageHp(Area2D area)
        {
            var shotAreaStrength = area as IHasStrength;
            var strengthToUse = shotAreaStrength?.ShotStrength ?? 0;
            Hp -= strengthToUse;
        }

        private void RemoveBullet(Area2D area)
        {
            var shotAreaBullet = area as PlayerBullet;
            shotAreaBullet?.QueueFree();
        }

        private void spawn_explosion()
        {
            var explosion = (Area2D)Explosion.Instance();
            explosion.Position = Position;
            // Spawn in parent so the item is not destroyed when this enemy is destroyed
            GetParent().AddChild(explosion);
        }

        /// <summary>
        /// Spawns instances of the item based on the multiplier.
        /// </summary>
        /// <param name="multiplier"></param>
        private void drop_item(float multiplier)
        {
            if (DropItem)
            {
                for(int i = 0; i <= multiplier; i++)
                {
                    var item = (RigidBody2D)ItemDrop.Instance();
                    GetParent().AddChild(item); // Spawn in parent so the item is not destroyed when this enemy is destroyed
                    item.Position = Position;
                }
            }
        }

    }
}
