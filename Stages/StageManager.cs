using Godot;
using Shmupper.Utilities;
using StageConfigLoader.Impl;
using System.Collections.Generic;
using System.Reflection;

/// <summary>
/// Manages the enemy spawn order and intervals from configuration.
/// </summary>
public class StageManager : Node
{
    [Export]
    public string StageNumber;

    private const string MOB_SPAWN_TIMER_RESOURCE = "MobSpawnTimer";

    private Queue<QueuedEnemyResource> _enemyQueue;
    private Timer _mobSpawnTimer;

    public override void _Ready()
    {
        base._Ready();
        SetupEnemyQueue();
        SetupMobSpawnTimer();
    }

    private void SetupEnemyQueue()
    {
        var stageLoader = new JsonStageConfigLoader();
        var stagePath = string.Format(StageConstants.STAGE_RESOURCE_TEMPLATE, StageNumber);
        _enemyQueue = stageLoader.LoadEnemyQueue(stagePath, Assembly.GetExecutingAssembly());
    }

    /// <summary>
    /// Obtains and starts the timer that will manage mob spawning
    /// </summary>
    private void SetupMobSpawnTimer()
    {
        _mobSpawnTimer = GetParent().GetNode<Timer>(MOB_SPAWN_TIMER_RESOURCE);
        _mobSpawnTimer.Start();
    }

    public bool IsEnemyQueueEmpty()
    {
        return _enemyQueue.Count <= 0; 
    }

    /// <summary>
    /// Executes in response to MobSpawnTimer reset.
    /// Creates a new enemy from the _enemyQueue
    /// </summary>
    public void _on_MobSpawnTimer_timeout()
    {
        if(!IsEnemyQueueEmpty())
        {
            // Get enemy resource and set timer
            var enemyResource = _enemyQueue.Dequeue();
            _mobSpawnTimer.WaitTime = enemyResource.TimeToNextLoad;

            // Add enemy to scene
            var enemyScene = GD.Load<PackedScene>(enemyResource.EnemyScene.PathToScene);
            var enemyInstance = enemyScene.Instance();
            GetParent().AddChild(enemyInstance);

            // Restart the timer
            _mobSpawnTimer.Start();
        }
    }
}