using Godot;
using Shmupper.Player.Impl;
using Shmupper.SharedInterfaces;
using Shmupper.Utilities;

namespace Shmupper.Items.Impl
{
    /// <summary>
    /// Represents a collectible.
    /// This is a RigidBody2D since it requires physics to run on the Scene this is applied to.
    /// </summary>
    public class CollectibleBase : RigidBody2D, IHasScore, IHasExperience
    {
        [Export]
        public int ExpGain = 10;

        /// <summary>
        /// Required to standardize experience points, since experience was represented in different properties in the original GDScript implementation.
        /// </summary>
        public int InternalExperience
        {
            get { return ExpGain;  }
            set { ExpGain = value; }
        }

        /// <summary>
        /// How quickly to home on the player
        /// </summary>
        [Export]
        public int HomingSpeed = 1;

        [Export]
        public int Score = 100;

        public int InternalScore {
            get { return Score; }
            set { Score = value; }
        }

        private Vector2 _playerVector = new Vector2(0, 0);
        private bool _playerTargeted = false;

        private GlobalPlayerStats _globalPlayerStats;

        public override void _Ready()
        {
            base._Ready();
            GetNode("VisibilityNotifier2D").Connect("screen_exited", this, "_on_screen_exited");
            RandomizeTrajectory();
            _playerTargeted = false;
            _globalPlayerStats = StageConstants.GetGlobalPlayerStatsNode(this);
        }

        /// <summary>
        /// Randomizes how this collectible is launched upon spawning
        /// </summary>
        private void RandomizeTrajectory()
        {
            var rng = new RandomNumberGenerator();
            rng.Randomize();
            LinearVelocity = new Vector2(rng.RandfRange(-50, 50), rng.RandfRange(0, -100));
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            if (_playerTargeted)
                HomeOnPlayer();
        }

        private void HomeOnPlayer()
        {
            _playerVector = _globalPlayerStats.GetDirectionToPlayer(this);
            Position += _playerVector * HomingSpeed;
        }

        public void _on_screen_exited()
        {
            _playerTargeted = false;
            QueueFree();
        }

        /// <summary>
        /// In response to the Timer node on the scene this is attached to, this sets the _playerTargeted to true when the timer runs out. 
        /// This allows the collectible to spread out randomly for a set amount of time before homing on the player.
        /// </summary>
        public void _on_Timer_timeout()
        {
            _playerTargeted = true;
            var timer = (Timer)GetNode("HomingTimer");
            timer.Start();
        }

        /// <summary>
        /// In response to the Timer node timing out, this will cause the collectible to home on the player.
        /// </summary>
        public void _on_HomingTimer_timeout()
        {
            _playerTargeted = false;
            LinearVelocity = _playerVector;
        }
    }
}
