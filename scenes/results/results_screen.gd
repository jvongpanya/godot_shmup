extends Node2D

var ScoreLabel

func _ready():
	ScoreLabel = $CanvasLayer/MarginContainer/VBoxContainer/Score
	ScoreLabel.text = str(GlobalPlayerStats.CurrentScore)
	
func _process(delta):
	if Input.is_action_just_pressed("ui_select"):
		GlobalPlayerStats.ResetModifiableStats()
		get_tree().change_scene("res://scenes/stages/stage_0/stage_0.tscn")