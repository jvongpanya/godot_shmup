﻿using Godot;
using Shmupper.Player.Impl;
using Shmupper.Utilities;

namespace Shmupper.scenes.hud
{
    /// <summary>
    /// Represents the main HUD for the game
    /// This is a CanvasLayer, which is a UI object in Godot
    /// </summary>
    public class MainHud : CanvasLayer
    {
        /// <summary>
        /// Player life sprite
        /// </summary>
        [Export]
        public PackedScene LifeTexture;

        /// <summary>
        /// Player Hellfire texture
        /// </summary>
        [Export]
        public PackedScene HellfireTexture;

        /// <summary>
        /// Player AngelGrace texture
        /// </summary>
        [Export]
        public PackedScene AngelGraceTexture;

        private int _maxAngelGraceLevel;
        private int _maxHellfireLevel;

        private TextureProgress _angelGraceGauge;
        private TextureProgress _hellfireGauge;
        private Label _scoreLabel;

        private HBoxContainer _livesContainers;
        private HBoxContainer _hellfireContainer;
        private HBoxContainer _angelGraceContainer;

        private GlobalPlayerStats _globalPlayerStats;

        public override void _Ready()
        {
            base._Ready();
            _globalPlayerStats = StageConstants.GetGlobalPlayerStatsNode(this);
            SetupGui();
            SetInitialStats();
        }

        /// <summary>
        /// Initializes all GUI elements
        /// </summary>
        private void SetupGui()
        {
            _angelGraceGauge = (TextureProgress)GetNode("BottomGUI/VBoxContainer/HBoxContainer/AngelGraceGauge");
            _hellfireGauge = (TextureProgress)GetNode("BottomGUI/VBoxContainer/HBoxContainer/HellfireGauge");
            _scoreLabel = (Label)GetNode("TopGUI/VBoxContainer/HBoxContainer/ScoreLabel");

            _livesContainers = (HBoxContainer)GetNode("TopGUI/VBoxContainer/HBoxContainer2");
            _hellfireContainer = (HBoxContainer)GetNode("Icons/HBoxContainer2/HellfireIcons");
            _angelGraceContainer = (HBoxContainer)GetNode("Icons/HBoxContainer2/AngelGraceIcons");
    }

        /// <summary>
        /// Initializes all stats
        /// </summary>
        private void SetInitialStats()
        {
            _angelGraceGauge.MaxValue = _globalPlayerStats.AngelGraceMaxLevel;
            _hellfireGauge.MaxValue = _globalPlayerStats.HellfireMaxLevel;
            _maxAngelGraceLevel = _globalPlayerStats.AngelGraceMaxLevel;
            _maxHellfireLevel = _globalPlayerStats.HellfireMaxLevel;
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            UpdateScore(_globalPlayerStats.CurrentScore);
            UpdateHellfire(_globalPlayerStats.Hellfire);
            UpdateHellfireLevel(_globalPlayerStats.HellfireLevel, _globalPlayerStats.HellfireLevelThreshold);
            UpdateAngelGrace(_globalPlayerStats.AngelGrace);
            UpdateAngelGraceLevel(_globalPlayerStats.AngelGraceLevel, _globalPlayerStats.AngelGraceLevelThreshold);
            UpdateIcons(_globalPlayerStats.Lives, _livesContainers, LifeTexture);
        }

        private void UpdateScore(int score)
        {
            UpdateLabel(_scoreLabel, score.ToString());
        }

        /// <summary>
        /// Updates Hellfire experience
        /// </summary>
        /// <param name="hellfire"></param>
        private void UpdateHellfire(int hellfire)
        {
            UpdateGauge(_hellfireGauge, hellfire);
        }

        /// <summary>
        /// Updates AngelGrace experience
        /// </summary>
        /// <param name="angelGrace"></param>
        private void UpdateAngelGrace(int angelGrace)
        {
            UpdateGauge(_angelGraceGauge, angelGrace);
        }

        /// <summary>
        /// Updates Hellfire level and display icons
        /// </summary>
        /// <param name="hellfireLevel"></param>
        /// <param name="newLevelThreshold"></param>
        private void UpdateHellfireLevel(int hellfireLevel, int newLevelThreshold)
        {
            UpdateLevelThreshold(_hellfireGauge, newLevelThreshold);
            UpdateIcons(hellfireLevel, _hellfireContainer, HellfireTexture);
        }

        /// <summary>
        /// Updates AngelGrace level and display icons
        /// </summary>
        /// <param name="angelGraceLevel"></param>
        /// <param name="newLevelThreshold"></param>
        private void UpdateAngelGraceLevel(int angelGraceLevel, int newLevelThreshold)
        {
            UpdateLevelThreshold(_angelGraceGauge, newLevelThreshold);
            UpdateIcons(angelGraceLevel, _angelGraceContainer, AngelGraceTexture);
        }
        
        /// <summary>
        /// Updates the provided container to show a the provided number of icons
        /// </summary>
        /// <param name="newValue">How many icons to show in the container</param>
        /// <param name="container">Container to store icons</param>
        /// <param name="texture">Icon to add to the container</param>
        private void UpdateIcons(int newValue, HBoxContainer container, PackedScene texture)
        {
            var textureChildren = container.GetChildren();

            if (newValue > textureChildren.Count)
            {
                var newLife = texture.Instance();
                container.AddChild(newLife);
            }

            if (newValue < textureChildren.Count)
            {
                container.GetChild(0).QueueFree();
            }
        }

        /// <summary>
        /// Updates the provided label with the newValue
        /// </summary>
        /// <param name="label"></param>
        /// <param name="newValue"></param>
        private void UpdateLabel(Label label, string newValue)
        {
            label.Text = newValue;
        }

        /// <summary>
        /// Updates the provided gauge's value with the newValue
        /// </summary>
        /// <param name="gauge"></param>
        /// <param name="newValue"></param>
        private void UpdateGauge(TextureProgress gauge, float newValue)
        {
            gauge.Value = newValue;
        }

        /// <summary>
        /// Updates the provided gauge's MaxValue with the threshold
        /// </summary>
        /// <param name="gauge"></param>
        /// <param name="threshold"></param>
        private void UpdateLevelThreshold(TextureProgress gauge, float threshold)
        {
            gauge.MaxValue = threshold;
        }

    }
}
