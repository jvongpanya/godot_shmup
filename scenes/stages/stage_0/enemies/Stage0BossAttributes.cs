﻿using Godot;
using Shmupper.Enemy;
using Shmupper.Utilities;

namespace Shmupper.scenes.stages.stage_0.enemies
{
    /// <summary>
    /// Represents the boss's attributes, which are special due to the boss having multiple phases
    /// </summary>
    public class Stage0BossAttributes : EnemyAttributesBase
    {
        [Export]
        public float Phase1Hp = 600;
        [Export]
        public float Phase2Hp = 600;
        [Export]
        public float Phase3Hp = 200;

        private float _totalHp;

        public bool InPhase2 { get; set; }

        public bool InPhase3 { get; set; }

        public override void _Ready()
        {
            base._Ready();
            Hp = Phase1Hp + Phase2Hp + Phase3Hp;
            _totalHp = Hp;
            IsBoss = true; // For base attributes to identify this as a boss correctly

            // TODO: Move this to a singleton song controller
            ((Stage0)StageConstants.GetStageNode(this)).ChangeToBossSong();
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            InPhase2 = Hp < _totalHp - Phase1Hp;
            InPhase3 = Hp < Phase3Hp;
        }
    }
}
