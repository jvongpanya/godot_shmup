using Godot;
using Shmupper.Bullet.Impl;
using Shmupper.Bullet.Interface;

namespace Shmupper.scenes.stages.stage_0.enemies
{
    public class Stage0BossShotsNode2 : Stage0BossShotsBase
    {
        #region Pattern1

        [Export]
        public PackedScene P1P1Shot;

        [Export]
        public float P1P1ShotSpeed = 1.5f;

        [Export]
        public int P1P1MaxShots = 5;

        [Export]
        public float P1P1MaxShotDelay = 1;

        [Export]
        public float P1P1ResetTime = 120;
        private int _p1P1CurrentReset = 0;

        private IEnemyBulletShooter _homingShooter;

        #endregion

        #region Pattern2

        [Export]
        public PackedScene P2P2Shot;

        [Export]
        public float P2P2ShotSpeed = 1;

        [Export]
        public int P2P2MaxShots = 200;

        [Export]
        public float P2P2MaxShotDelay = 3;

        [Export]
        public float P2P2SineShotIntervalDeg = 20;

        [Export]
        public bool P2P2SineCrossing = false;

        [Export]
        public float P2P2ResetTime = 5
            ;
        private float _p2P2CurrentReset = 0;

        [Export]
        public float P2P2PhaseTime = 330;
        private float _p2P2CurrentPhaseTime = 0;

        private IEnemyBulletShooter _sineShooter;

        #endregion

        public override void _Ready()
        {
            base._Ready();
            _homingShooter = new HomingBulletStepShooter(P1P1MaxShotDelay, P1P1MaxShots, GlobalPlayerStats, this);
            _sineShooter = new SineWaveStepShooter(P2P2MaxShotDelay, P2P2MaxShots, GlobalPlayerStats, this, P2P2SineShotIntervalDeg, P2P2SineCrossing);
        }

        // Done because base process method does not run
        public override void _Process(float delta)
        {
            base._Process(delta);
        }

        protected override void DoPhase1()
        {
            DoSingleHomingStream();
        }

        protected override void DoPhase2()
        {
            DoSingleHomingStream();
        }

        protected override void DoPhase3()
        {
            DoSineShots();
        }

        private void DoSingleHomingStream()
        {
            if (_p1P1CurrentReset >= P1P1ResetTime)
            {
                var homingShotProperties = new EnemyShotProperties();
                homingShotProperties.BulletSpeed = P1P1ShotSpeed;
                homingShotProperties.ShotAsset = P1P1Shot;

                bool shotsDone = _homingShooter.StepShoot(homingShotProperties);

                if(shotsDone)
                    _p1P1CurrentReset = 0;
            }
            else
            {
                _homingShooter.ResetShotParameters();
                _p1P1CurrentReset++;
            }
        }

        private void DoSineShots()
        {
            if (_p2P2CurrentReset >= P2P2ResetTime)
            {
                if (_p2P2CurrentPhaseTime > 0)
                {
                    var sineShotProperties = new EnemyShotProperties();
                    sineShotProperties.BulletSpeed = P2P2ShotSpeed;
                    sineShotProperties.ShotAsset = P2P2Shot;

                    _sineShooter.StepShoot(sineShotProperties);
                    _p2P2CurrentPhaseTime -= 1;
                }
                else
                {
                    _p2P2CurrentReset = 0;
                    _sineShooter.ResetShotParameters();
                    _p2P2CurrentPhaseTime = P2P2PhaseTime;
                }
            }
            else
            {
                _p2P2CurrentReset++;
            }
        }

        public void _on_PathFollow2D_path_complete()
        {
            base._on_EnemyPathFollow_path_complete();
        }
    }
}
