﻿using Shmupper.Bullet.Impl;

namespace Shmupper.scenes.stages.stage_0.enemies
{
    public abstract class Stage0BossShotsBase : EnemyShotBehaviorBase
    {
        protected bool StartShooting = false;

        protected Stage0BossAttributes BossAttributes;

        public override void _Ready()
        {
            base._Ready(); // Required since the base ready does not actually run
            BossAttributes = (Stage0BossAttributes)GetParent();
        }

        public override void _Process(float delta)
        {
            base._Process(delta); // Required since the base process does not actually run
            if (StartShooting)
            {
                if (BossAttributes.InPhase3)
                    DoPhase3();
                else if (BossAttributes.InPhase2)
                    DoPhase2();
                else
                    DoPhase1();
            }
        }

        protected abstract void DoPhase1();
        protected abstract void DoPhase2();
        protected abstract void DoPhase3();

        /// <summary>
        /// Event that allows enemy to start shooting as soon as its EnemyPathFollow node completes
        /// </summary>
        public void _on_EnemyPathFollow_path_complete()
        {
            StartShooting = true;
        }


        /// <summary>
        /// Event that stops enemy from shooting as soon as its PathFollow2D node completes
        /// </summary>
        public void _on_PathFollow2D_leaving_screen()
        {
            StartShooting = false;
        }

    }
}
