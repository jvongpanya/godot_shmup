using Godot;
using Shmupper.Bullet.Impl;
using Shmupper.Bullet.Interface;

namespace Shmupper.scenes.stages.stage_0.enemies
{
    public class BigDemon5 : EnemyShotBehaviorBase
    {
        [Export]
        public float ShotSpeed = 1.5f;

        [Export]
        public PackedScene Shot;

        [Export]
        public float MaxShotDelay = 8;

        [Export]
        public int MaxShots = 5;

        [Export]
        public int NumberToShoot = 3;

        [Export]
        public float ShotIntervalDeg = 10;

        private bool _startShooting = false;

        private IEnemyBulletShooter _spreadShooter;

        public override void _Ready()
        {
            base._Ready();
            _spreadShooter = new SpreadShotStepShooter(MaxShotDelay, MaxShots, GlobalPlayerStats, this, ShotIntervalDeg, NumberToShoot);
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            if (_startShooting)
            {
                var shotProperties = new EnemyShotProperties();
                shotProperties.BulletSpeed = ShotSpeed;
                shotProperties.ShotAsset = Shot;
                _spreadShooter.StepShoot(shotProperties);
            }
        }
		
		public void _on_EnemyPathFollow_path_complete()
		{
			_startShooting = true;	
		}


    }
}
