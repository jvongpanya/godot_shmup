﻿using Godot;
using Shmupper.Bullet.Impl;
using Shmupper.Bullet.Interface;

namespace Shmupper.scenes.stages.stage_0.enemies
{
    public class AngelSwarm3 : EnemyShotBehaviorBase
    {
        [Export]
        public float ShotSpeed = 1.5f;

        [Export]
        public PackedScene Shot;

        private IEnemyBulletShooter _homingBulletShooter;

        public override void _Ready()
        {
            base._Ready();
            GetParent().GetParent().GetParent().GetNode("VisibilityNotifier2D").Connect("screen_entered", this, "shoot_screen_enter");
            _homingBulletShooter = new HomingBulletStepShooter(0, 1, GlobalPlayerStats, this);
        }

        public void shoot_screen_enter()
        {
            var timer = (Timer)GetNode("Timer");
            timer.Start();
        }

        public void _on_Timer_timeout()
        {
            var shotProperties = new EnemyShotProperties();
            shotProperties.BulletSpeed = ShotSpeed;
            shotProperties.ShotAsset = Shot;
            _homingBulletShooter.StepShoot(shotProperties);
        }
    }
}
