﻿using Godot;
using Shmupper.Bullet.Impl;
using Shmupper.Bullet.Interface;

namespace Shmupper.scenes.stages.stage_0.enemies
{
    public class MidbossDemon12 : EnemyShotBehaviorBase
    {
        private bool _startShooting = false;

        #region Pattern Management
        [Export]
        public float TimeBetweenPatterns = 180;

        private float _currentTimeBetweenPatterns = 0;

        private int _pattern = 1;
        #endregion

        #region Pattern 1
        // Spiral Shot
        [Export]
        public PackedScene SpiralShot;

        [Export]
        public int MaxSpiralShots = 200;

        [Export]
        public float MaxSpiralShotDelay = 5;

        [Export]
        public float SpiralShotSpeed = 1.5f;

        [Export]
        public int NumberOfSpiralPairs = 2;

        [Export]
        public int PairInterval = 90;

        [Export]
        public int MainRateInterval = 10;

        private IEnemyBulletShooter _spiralShooter1;

        // Homing Shot
        [Export]
        public PackedScene HomingShot;

        [Export]
        public int MaxHomingShots = 15;

        [Export]
        public float MaxHomingShotDelay = 20;

        [Export]
        public float HomingShotSpeed = 1.25f;

        private IEnemyBulletShooter _homingShooter;

        #endregion

        #region Pattern 2
        // Spiral Shot
        [Export]
        public PackedScene SpiralShot2;

        [Export]
        public int MaxSpiralShots2 = 8;

        [Export]
        public float MaxSpiralShotDelay2 = 3;

        [Export]
        public float SpiralShotSpeed2 = 2f;

        [Export]
        public int NumberOfSpiralPairs2 = 3;

        [Export]
        public int PairInterval2 = 10;

        [Export]
        public int MainRateInterval2 = 30;

        [Export]
        public float SpiralShot2TweenTime = 0.5f;

        private IEnemyBulletShooter _spiralShooter2;
        #endregion

        public override void _Ready()
        {
            base._Ready();
            _spiralShooter1 = new SpiralStepShooter(MaxSpiralShotDelay, MaxSpiralShots, GlobalPlayerStats, this, NumberOfSpiralPairs, PairInterval, MainRateInterval, true);
            _spiralShooter2 = new SpiralStepShooter(MaxSpiralShotDelay2, MaxSpiralShots2, GlobalPlayerStats, this, NumberOfSpiralPairs2, PairInterval2, MainRateInterval2, true);
            _homingShooter = new HomingBulletStepShooter(MaxHomingShotDelay, MaxHomingShots, GlobalPlayerStats, this);
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            if (_startShooting)
            {
                if (_currentTimeBetweenPatterns >= TimeBetweenPatterns)
                {
                    if (_pattern == 1)
                    {
                        var result = DoPattern1();
                        if (result)
                        {
                            _currentTimeBetweenPatterns = 0;
                            _pattern = 2;
                            _spiralShooter1.ResetShotParameters();
                            _homingShooter.ResetShotParameters();
                        }
                    }
                    else
                    {
                        var result = DoPattern2();
                        if(result)
                        {
                            _currentTimeBetweenPatterns = 0;
                            _pattern = 1;
                            _spiralShooter2.ResetShotParameters();
                        }
                    }
                }
                else
                {
                    _currentTimeBetweenPatterns++;
                }
            }
        }

        private bool DoPattern1()
        {
            var spiralShotProperties = new EnemyShotProperties();
            spiralShotProperties.BulletSpeed = SpiralShotSpeed;
            spiralShotProperties.ShotAsset = SpiralShot;
             _spiralShooter1.StepShoot(spiralShotProperties);

            var homingShotProperties = new EnemyShotProperties();
            homingShotProperties.BulletSpeed = HomingShotSpeed;
            homingShotProperties.ShotAsset = HomingShot;
            var result = _homingShooter.StepShoot(homingShotProperties);

            return result;
        }

        private bool DoPattern2()
        {
            var spiralShotProperties = new EnemyShotProperties();
            spiralShotProperties.BulletSpeed = SpiralShotSpeed2;
            spiralShotProperties.ShotAsset = SpiralShot2;
            return _spiralShooter2.StepShoot(spiralShotProperties);
        }

        public void _on_EnemyPathFollow_path_complete()
        {
            _startShooting = true;
        }

        public void _on_PathFollow2D_leaving_screen()
        {
            _startShooting = false;
        }




    }
}
