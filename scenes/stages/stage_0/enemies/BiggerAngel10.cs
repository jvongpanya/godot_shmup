﻿using Godot;
using Shmupper.Bullet.Impl;
using Shmupper.Bullet.Interface;

namespace Shmupper.scenes.stages.stage_0.enemies
{
    public class BiggerAngel10 : EnemyShotBehaviorBase
    {
        private bool _startShooting = false;

        #region Sine Shot
        [Export]
        public PackedScene SineShot;

        [Export]
        public int MaxSineShots = 20;

        [Export]
        public float MaxSineShotDelay = 1;

        [Export]
        public float SineShotSpeed = 1.5f;

        [Export]
        public float SineShotIntervalDeg = 20;

        private IEnemyBulletShooter _sineShooter;
        #endregion

        #region Straight shot
        [Export]
        public PackedScene Shot;

        [Export]
        public int NumberToShoot = 3;

        [Export]
        public float MaxShotDelay = 1;

        [Export]
        public int MaxShots = 1;

        [Export]
        public float ShotSpeed = 1.5f;

        [Export]
        public float ShotIntervalDeg = 10;

        private IEnemyBulletShooter _straightShooter;
        #endregion

        public override void _Ready()
        {
            base._Ready();
            _sineShooter = new SineWaveStepShooter(MaxSineShotDelay, MaxSineShots, GlobalPlayerStats, this, SineShotIntervalDeg, true);
            _straightShooter = new SpreadShotStepShooter(MaxShotDelay, MaxShots, GlobalPlayerStats, this, ShotIntervalDeg, NumberToShoot);
        }


        public override void _Process(float delta)
        {
            base._Process(delta);
            if (_startShooting)
            {
                var shotPropertiesStraight = new EnemyShotProperties();
                shotPropertiesStraight.ShotAsset = Shot;
                shotPropertiesStraight.BulletSpeed = ShotSpeed;
                _straightShooter.StepShoot(shotPropertiesStraight);

                var shotPropertiesSine = new EnemyShotProperties();
                shotPropertiesSine.ShotAsset = SineShot;
                shotPropertiesSine.BulletSpeed = SineShotSpeed;
                _sineShooter.StepShoot(shotPropertiesSine);
            }
        }

        public void _on_EnemyPathFollow_path_complete()
        {
            _startShooting = true;
        }
    }
}
