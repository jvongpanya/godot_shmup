﻿using Godot;
using Shmupper.Bullet.Impl;
using Shmupper.Bullet.Interface;

namespace Shmupper.scenes.stages.stage_0.enemies
{
    public class Angel13 : EnemyShotBehaviorBase
    {
        [Signal]
        public delegate void shots_complete();

        [Export]
        public PackedScene Shot;

        [Export]
        public float ShotInterval = 10;

        [Export]
        public int MaxShots = 5;

        [Export]
        public float ShotSpeed = 1.5f;

        [Export]
        public float WaitBeforeLeave = 30;
        private float _currentWaitBeforeLeave = 0;

        private bool _startShooting = false;
        private bool _doneShooting = false;

        private IEnemyBulletShooter _homingBulletShooter;

        public override void _Ready()
        {
            base._Ready();
            _homingBulletShooter = new HomingBulletStepShooter(ShotInterval, MaxShots, GlobalPlayerStats, this);
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            bool maxShotsReached = false;
            if (_startShooting)
            {
                var shotProperties = new EnemyShotProperties();
                shotProperties.BulletSpeed = ShotSpeed;
                shotProperties.ShotAsset = Shot;
                maxShotsReached = _homingBulletShooter.StepShoot(shotProperties);
            }

            if (maxShotsReached)
            {
                _currentWaitBeforeLeave++;
            }

            if (_currentWaitBeforeLeave >= WaitBeforeLeave && !_doneShooting)
            {
                EmitSignal(nameof(shots_complete));
                _doneShooting = true;
            }
        }

        public void _on_EnemyPathFollow_path_complete()
        {
            _startShooting = true;
        }

    }
}
