extends CanvasLayer

var boss;

# Called when the node enters the scene tree for the first time.
func _ready():
	boss = get_parent().get_node("PathFollow2D/Enemy")
	$MarginContainer/BossHealthGauge.max_value = boss.Hp

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$MarginContainer/BossHealthGauge.value = boss.Hp
