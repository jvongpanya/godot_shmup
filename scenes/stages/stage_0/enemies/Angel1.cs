using Godot;
using Shmupper.Bullet.Impl;
using Shmupper.Bullet.Interface;

namespace Shmupper.scenes.stages.stage_0.enemies
{
    public class Angel1 : EnemyShotBehaviorBase
    {
        [Export]
        public float ShotSpeed = 1.5f;

        [Export]
        public PackedScene Shot;

        [Export]
        public float ShotInterval = 10;

        [Export]
        public int MaxShots = 5;

        private bool _startShooting = false;

        private IEnemyBulletShooter _homingBulletShooter;

        public override void _Ready()
        {
            base._Ready();
            _homingBulletShooter = new HomingBulletStepShooter(ShotInterval, MaxShots, GlobalPlayerStats, this);
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            if (_startShooting)
            {
                var shotProperties = new EnemyShotProperties();
                shotProperties.BulletSpeed = ShotSpeed;
                shotProperties.ShotAsset = Shot;
                _homingBulletShooter.StepShoot(shotProperties);
            }
        }
		
		public void _on_EnemyPathFollow_path_complete() 
		{
			_startShooting = true;
		}
    }
}
