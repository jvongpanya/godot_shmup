using Godot;
using Shmupper.Bullet.Impl;
using Shmupper.Bullet.Interface;

namespace Shmupper.scenes.stages.stage_0.enemies
{

    public class Stage0BossShotsNode1 : Stage0BossShotsBase
    {

        #region Pattern1 

        [Export]
        public PackedScene P1P1Shot;

        [Export]
        public float P1P1ShotSpeed = 1.5f;

        [Export]
        public int P1P1MaxShots = 200;

        [Export]
        public float P1P1MaxShotDelay = 10;

        [Export]
        public float P1P1SineShotIntervalDeg = 15;

        [Export]
        public bool P1P1SineCrossing = false;

        [Export]
        public float P1P1ResetTime = 180;
        private float _p1P1CurrentReset = 0;

        [Export]
        public float P1P1PhaseTime = 600;
        private float _p1P1CurrentPhaseTime = 0;

        private IEnemyBulletShooter _sineShooter1;
        #endregion

        #region Pattern2

        [Export]
        public PackedScene P2P2Shot;

        [Export]
        public float P2P2ShotSpeed = 3f;

        [Export]
        public int P2P2MaxShots = 200;

        [Export]
        public float P2P2MaxShotDelay = 3;

        [Export]
        public float P2P2SineShotIntervalDeg = 17;

        [Export]
        public bool P2P2SineCrossing = false;

        [Export]
        public float P2P2ResetTime = 180;
        private float _p2P2CurrentReset = 0;

        [Export]
        public float P2P2PhaseTime = 420;
        private float _p2P2CurrentPhaseTime = 0;

        private IEnemyBulletShooter _sineShooter2;
        #endregion

        #region Pattern3
        [Export]
        public PackedScene P3P3Shot;

        [Export]
        public float P3P3ShotSpeed = 1f;

        [Export]
        public int P3P3NumberToShoot = 8;

        [Export]
        public float P3P3BulletIntervalDeg = 15;

        [Export]
        public int P3P3MaxShots = 200;

        [Export]
        public float P3P3MaxShotDelay = 60;

        [Export]
        public float P3P3ResetTime = 40;
        private float _p3P3CurrentReset = 0;

        [Export]
        public float P3P3PhaseTime = 300;
        private float _p3P3CurrentPhaseTime = 0;

        private IEnemyBulletShooter _spreadShooter;
        #endregion

        public override void _Ready()
        {
            base._Ready();
            _sineShooter1 = new SineWaveStepShooter(P1P1MaxShotDelay, P1P1MaxShots, GlobalPlayerStats, this, P1P1SineShotIntervalDeg, P1P1SineCrossing);
            _sineShooter2 = new SineWaveStepShooter(P2P2MaxShotDelay, P2P2MaxShots, GlobalPlayerStats, this, P2P2SineShotIntervalDeg, P2P2SineCrossing);
            _spreadShooter = new SpreadShotStepShooter(P3P3MaxShotDelay, P3P3MaxShots, GlobalPlayerStats, this, P3P3BulletIntervalDeg, P3P3NumberToShoot);
        }

        public override void _Process(float delta)
		{
			base._Process(delta); // Done because base process method does not run
        }

        protected override void DoPhase1()
        {
            DoSineShots1();
        }

        protected override void DoPhase2()
        {
            DoSineShots2();
        }

        protected override void DoPhase3()
        {
            DoFinalPhase();
        }

        private void DoSineShots1()
        {
            if (_p1P1CurrentReset >= P1P1ResetTime) // Only shoot if not in the "rest" period, indicated as "reset" here
            {
                if (_p1P1CurrentPhaseTime > 0)
                { 
                    // Shoot bullets as long as we are in the phase 
                    var sineShotProperties = new EnemyShotProperties();
                    sineShotProperties.BulletSpeed = P1P1ShotSpeed;
                    sineShotProperties.ShotAsset = P1P1Shot;
                    _sineShooter1.StepShoot(sineShotProperties);
                    _p1P1CurrentPhaseTime--;
                }
                else
                {
                    // Put into rest mode, and reset shots 
                    _sineShooter1.ResetShotParameters();
                    _p1P1CurrentReset = 0;
                    _p1P1CurrentPhaseTime = P1P1PhaseTime;
                }
            }
            else
            {
                _p1P1CurrentReset++;
            }
        }

        private void DoSineShots2()
        {
            if (_p2P2CurrentReset >= P2P2ResetTime)
            {
                if (_p2P2CurrentPhaseTime > 0)
                {
                    var sineShotProperties = new EnemyShotProperties();
                    sineShotProperties.BulletSpeed = P2P2ShotSpeed;
                    sineShotProperties.ShotAsset = P2P2Shot;
                    _sineShooter2.StepShoot(sineShotProperties);
                    _p2P2CurrentPhaseTime--;
                }
                else
                {
                    _p2P2CurrentReset = 0;
                    _sineShooter2.ResetShotParameters();
                    _p2P2CurrentPhaseTime = P2P2PhaseTime;
                }
            }
            else
            {
                _p2P2CurrentReset++;
            }
        }

        private void DoFinalPhase()
        {
            if (_p3P3CurrentReset >= P3P3ResetTime)
            {
                if (_p3P3CurrentPhaseTime > 0)
                {
                    var spreadShotProperties = new EnemyShotProperties();
                    spreadShotProperties.BulletSpeed = P3P3ShotSpeed;
                    spreadShotProperties.ShotAsset = P3P3Shot;

                    _spreadShooter.StepShoot(spreadShotProperties);
                    _p3P3CurrentPhaseTime--;
                }
                else
                {
                    _p3P3CurrentReset = 0;
                    _spreadShooter.ResetShotParameters();
                    _p3P3CurrentPhaseTime = P3P3PhaseTime;
                }
            }
            else
            {
                _p3P3CurrentReset++;
            }
        }

        public void _on_PathFollow2D_path_complete()
        {
            _on_EnemyPathFollow_path_complete();
        }
    }
}
