using Godot;
using Shmupper.Player.Impl;
using Shmupper.Utilities;

namespace Shmupper.scenes.stages.stage_0
{
    /// <summary>
    /// Represents the stage behavior
    /// </summary>
    public class Stage0 : Node
    {
        private const string RESET_KEY = "reset";
        private const string PLAYER_NODE = "Player";

        private GlobalPlayerStats _globalPlayerStats;
        private MarginContainer _resetContainer;

        private Tween _musicTween;
        private AudioStreamPlayer _stageSong;  
        private AudioStreamPlayer _bossSong;  
        private AudioStreamPlayer _victorySong;  

        public override void _Ready()
        {
            base._Ready();
            _globalPlayerStats = StageConstants.GetGlobalPlayerStatsNode(this);
            _resetContainer = (MarginContainer)GetNode("Reset/ResetContainer");
            _stageSong = GetNode<AudioStreamPlayer>("Audio/StageSong");
            _bossSong = GetNode<AudioStreamPlayer>("Audio/BossSong");
            _musicTween = GetNode<Tween>("Audio/Tween");
            _resetContainer.Visible = false;
        }

        /// <summary>
        /// Interpolates the song volume of the provided song type in our out, by the length of the transitionTime.
        /// Todo: Move song handling to singleton
        /// </summary>
        /// <param name="tweenIn">Whether to tween this song in</param>
        /// <param name="songEnum">The song type</param>
        /// <param name="transitionTime">How long the tween lasts, in seconds</param>
        /// <param name="delay">How long to delay, in seconds</param>
        private void TweenSong(bool tweenIn, SongType songEnum = SongType.Stage, float transitionTime = 1f, float delay = 0)
        {
            AudioStreamPlayer song;
            switch(songEnum)
            {
                case SongType.Boss:
                    song = _bossSong;
                    break;
                default:
                    song = _stageSong;
                    break;
            }
            _musicTween.InterpolateProperty(
                song,
                "volume_db",
                tweenIn ? -80 : 0,
                tweenIn ? 0 : -80,
                transitionTime,
                Tween.TransitionType.Linear,
                Tween.EaseType.In,
                delay);
            _musicTween.Repeat = false;
            if(tweenIn) song.Play(); // Only play if the song is meant to fade in
            _musicTween.Start();
        }

        public void ChangeToBossSong()
        {
            TweenSong(false, SongType.Stage, 2f, 0);
            TweenSong(true, SongType.Boss, 0.75f, 1f);
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
			CheckPlayerDestroyed(); 
        }

        /// <summary>
        /// This fires in response to an event that calls end_stage()
        /// </summary>
        public void end_stage()
        {
            TweenSong(false, SongType.Boss, 2, 0);
            ((Timer)GetNode("ResetStageTimer")).Start(); // Starts the timer node
        }

        /// <summary>
        /// If the player is destroyed, allow the player to reset
        /// </summary>
        private void CheckPlayerDestroyed()
        {
            if (!HasNode(PLAYER_NODE))
            {
                _resetContainer.Visible = true;
                var resetPressed = Input.IsActionPressed(RESET_KEY);
                if (resetPressed)
                {
                    GetTree().ReloadCurrentScene();
                    _globalPlayerStats.ResetModifiableStats();
                }
            }
        }

        /// <summary>
        /// Todo: Move song handling to a new class or singleton
        /// </summary>
        internal enum SongType
        {
            Stage,
            Boss,
            Victory
        }
    }
}
