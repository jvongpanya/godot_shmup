extends PathFollow2D

class_name EnemyPathFollowBase

# Signals
signal path_complete
signal leaving_screen

onready var leaving = false

export var leave_screen = true
export var wait_before_leave = 30
export var leave_speed = 0.3
export var leave_down = true
var reached_end = false
var current_wait = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	handle_path_completion(delta)

# Handles action when the path is complete, and after it is complete
func handle_path_completion(delta):
	if !reached_end && unit_offset == 1:
		reached_end = true
		emit_signal("path_complete")
	elif reached_end:
		current_wait += 1
		if has_node("Enemy"):
			var enemy_child = get_node("Enemy")
			# If the enemy is set and ready to leave the screen, slowly drift off using the leave_speed
			if(enemy_child != null && current_wait >= wait_before_leave && leave_screen):
				var leave_direction = -1
				if(!leave_down):
					leave_direction = 1
				get_parent().position -= Vector2(0, leave_direction) * leave_speed
				if !leaving:
					emit_signal("leaving_screen")
					leaving = true
