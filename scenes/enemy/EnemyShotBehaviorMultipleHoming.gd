extends EnemyShotBehaviorBase

export (PackedScene) var shot

export var shot_interval = 10
onready var current_shot_timer = 0
export var max_shots = 5
onready var current_shots = 0
export var shot_speed = 1.5

# From base class
func _do_shot(delta):
	if current_shot_timer >= shot_interval && current_shots < max_shots:
		shoot_homing_bullet(shot_speed, shot)
		# Reset timer
		current_shot_timer = 0
		# Add to shots
		current_shots += 1
	else:
		current_shot_timer += 1
