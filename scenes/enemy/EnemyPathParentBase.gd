extends Path2D

# Parent of the PathFollow2D, and manipulates how the PathFollow2D moves
class_name EnemyPathParentBase

# Path follow
export var initial_speed = 0
export var final_speed = 1
export var transition_time = 2
export var delay = 0
export var repeat = false
onready var follow = get_node("PathFollow2D")
var tween

func _ready():
	set_process(true)
	tween = Tween.new()
	add_child(tween)
	tween.interpolate_property(follow,
		"unit_offset",
		initial_speed, final_speed, transition_time,
		tween.TRANS_LINEAR,
		tween.EASE_IN_OUT,
		delay)
	tween.set_repeat(repeat)
	tween.start()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
