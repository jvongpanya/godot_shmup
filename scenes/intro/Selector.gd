extends AnimatedSprite

var actionContainer;
var menuContainer;
var loadingContainer; 
var song;
var selectAudio;
var musicTween;

func _ready():
	actionContainer = get_parent().get_node("CanvasLayer/ActionContainer")
	menuContainer = get_parent().get_node("CanvasLayer/MenuContainer")
	loadingContainer = get_parent().get_node("CanvasLayer/LoadingContainer") 
	song = get_parent().get_node("Audio/Song")
	selectAudio = get_parent().get_node("Audio/Select")
	musicTween = get_parent().get_node("Audio/Tween")
	
func _process(delta):
	if Input.is_action_just_pressed("ui_select"):
		hide()
		selectAudio.play()
		actionContainer.hide()
		menuContainer.hide()
		loadingContainer.visible = true
		get_tree().change_scene("res://scenes/stages/stage_0/stage_0.tscn")
