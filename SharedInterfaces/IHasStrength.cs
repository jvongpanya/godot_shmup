﻿namespace Shmupper.SharedInterfaces
{
    public interface IHasStrength
    {
        float ShotStrength { get; set; }
    }
}
