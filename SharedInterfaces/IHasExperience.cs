﻿namespace Shmupper.SharedInterfaces
{
    /// <summary>
    /// Required to standardize experience, since the original GDScript implementation of the project had some differences in the experience property on some scenes.
    /// </summary>
    public interface IHasExperience
    {
        int InternalExperience { get; set; }
    }
}
