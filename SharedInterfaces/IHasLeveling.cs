﻿namespace Shmupper.SharedInterfaces
{
    public interface IHasLeveling
    {
        int CurrentLevel { get; }
        int CurrentExperience { get; }
        int CurrentExperienceThreshold { get; }
        int MaxLevel { get; }
        bool IncreaseExperienceWithThreshold(int amount);
        void DecreaseLevel(int amount);
    }
}
