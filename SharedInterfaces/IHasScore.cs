﻿namespace Shmupper.SharedInterfaces
{
    /// <summary>
    /// Required to standardize scores, since the original GDScript implementation of the project had some differences in the score property on some scenes.
    /// </summary>
    public interface IHasScore
    {
        int InternalScore { get; set; }
    }
}
