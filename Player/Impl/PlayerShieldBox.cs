using Godot;
using Shmupper.Bullet.Impl;
using Shmupper.Utilities;
using System;

namespace Shmupper.Player.Impl
{
    public class PlayerShieldBox : Area2D
    {
        public override void _Ready()
        {
            base._Ready();
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
        }

        /// <summary>
        /// Event that fires when the shieldbox area is entered
        /// </summary>
        /// <param name="area"></param>
        public void _on_ShieldBox_area_entered(Area2D area)
        {
            SlowEnemyBullet(true, area);
        }

        /// <summary>
        /// Event that fires when the shieldbox area is exited
        /// </summary>
        /// <param name="area"></param>
        public void _on_ShieldBox_area_exited(Area2D area)
        {
            SlowEnemyBullet(false, area);
        }

        /// <summary>
        /// If the area is an enemy bullet, sets the slow down based on slow
        /// </summary>
        /// <param name="slow"></param>
        /// <param name="area"></param>
        private void SlowEnemyBullet(bool slow, Area2D area)
        {
            if (area.IsInGroup(GroupConstants.ENEMY_BULLET_GROUP))
            {
                EnemyBulletBase enemyBullet = area as EnemyBulletBase;
                enemyBullet.SlowBullet(slow);
            }
        }
    }
	
}
