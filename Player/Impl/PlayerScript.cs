using Godot;
using Shmupper.Bullet.Impl;
using Shmupper.Enemy;
using Shmupper.Items.Impl;
using Shmupper.scenes.bullets.Impl;
using Shmupper.scenes.stages.stage_0;
using Shmupper.Utilities;
using System;

namespace Shmupper.Player.Impl
{
    /// <summary>
    /// Represents all actions and properties of the player.
    /// This is an Area2D to signify that it occupies space in the scene in the Godot engine
    /// </summary>
    public class PlayerScript : Area2D
    {

        #region Player Movement
        [Export]
        public int PlayerSpeed = 130;

        /// <summary>
        /// Player's slowed speed
        /// </summary>
        [Export]
        public int PlayerSpeedSlow = 70;

        /// <summary>
        /// The amount of frames before the player slows down
        /// </summary>
        [Export]
        public int TimeBeforeSlow = 30;

        private int _currentTimeBeforeSlow = 0;

        /// <summary>
        /// The amount of pixels from the bottom that the player cannot move into
        /// </summary>
        [Export]
        public int BottomBarAllowance = 25;

        private Vector2 _screenSize;

        #endregion

        #region Animations
        private const string DEFAULT_ANIMATION_NAME = "default";
        private const string RIGHT_ANIMATION_NAME = "right";
        private const string LEFT_ANIMATION_NAME = "left";
        [Export]
        public Texture PowerUpSprite;
        [Export]
        public Texture NormalSprite;

        private Sprite _sprite;
        #endregion

        #region Inputs
        private const string RIGHT_BUTTON = "ui_right";
        private const string LEFT_BUTTON = "ui_left";
        private const string DOWN_BUTTON = "ui_down";
        private const string UP_BUTTON = "ui_up";
        private const string HELLFIRE_SHOOT_BUTTON = "ui_select";
        private const string ANGEL_GRACE_SHOOT_BUTTON = "ui_cancel";
        private const string SPECIAL_ACTION_BUTTON = "special_action";
        #endregion

        #region Invincibility
        /// <summary>
        /// The frames for how long a player is invincible for
        /// </summary>
        [Export]
        public int InvincibilityTimeFrame = 60;

        /// <summary>
        /// The current number of frames the player has been invincible for
        /// </summary>
        private float _currentInvincibilityFrames = 0;
        #endregion

        #region Devil Trigger
        /// <summary>
        /// Devil Trigger time in frames
        /// </summary>
        [Export]
        public int DevilTriggerTime = 400;

        /// <summary>
        /// Current devil trigger time in frames
        /// </summary>
        private int _currentDevilTriggerTime = 0;

        /// <summary>
        /// Strength of bullets in devil trigger mode
        /// </summary>
        [Export]
        public float DevilTriggerBulletStrength = 2;

        /// <summary>
        /// The progress bar for the devil trigger
        /// </summary>
        TextureProgress _devilTriggerBar; 
        #endregion

        #region Shot params
        [Export]
        public PackedScene HellfireShot;
        [Export]
        public PackedScene AngelGraceShot;
        [Export]
        public PackedScene IridescentShot;

        /// <summary>
        /// The normal shot strength
        /// </summary>
        [Export]
        public float ShotStrength = 2f;

        /// <summary>
        /// The frames between each shot
        /// </summary>
        [Export]
        public int FramesBeforeNextShot = 20;

        /// <summary>
        /// The number of frames that shots continously fire after pressing a shot button
        /// </summary>
        [Export]
        public int ShotDuration = 1;

        /// <summary>
        /// The number of frames shots have shot for
        /// </summary>
        private int _currentShotDuration = 1;

        /// <summary>
        /// The number of frames before the next shot is allowede to fire
        /// </summary>
        private int _currentFramesBeforeNextShot = 0;

        [Export]
        public PackedScene HellfireBomb;
        [Export]
        public float HellfireBombStrength = 100;

        [Export]
        public PackedScene AngelBomb;
        #endregion

        private GlobalPlayerStats _globalPlayerStats;

        #region Audio
        private AudioStreamPlayer _levelUp; 
        private AudioStreamPlayer _transform; 
        private AudioStreamPlayer _death; 
        private AudioStreamPlayer _bomb;
        private AudioStreamPlayer _shoot;
        private AudioStreamPlayer _transformBack;
        #endregion

        public override void _Ready()
        {
            base._Ready();
            _screenSize = GetViewportRect().Size;
            _globalPlayerStats = StageConstants.GetGlobalPlayerStatsNode(this);
            _sprite = (Sprite)GetNode("Sprite");
            SetupDevilTrigger();
            SetupAudio();
        }

        private void SetupDevilTrigger()
        {
            _devilTriggerBar = (TextureProgress)GetNode("DevilTriggerIndicator/VBoxContainer/DevilTriggerGauge");
            _devilTriggerBar.MaxValue = DevilTriggerTime;
            _devilTriggerBar.Value = _currentDevilTriggerTime;
            _devilTriggerBar.Visible = false;
        }

        private void SetupAudio()
        {
            _levelUp = GetNode<AudioStreamPlayer>("SFX/LevelUp");
            _transform = GetNode<AudioStreamPlayer>("SFX/Transform");
            _death = GetNode<AudioStreamPlayer>("SFX/Death");
            _bomb = GetNode<AudioStreamPlayer>("SFX/Bomb");
            _shoot = GetNode<AudioStreamPlayer>("SFX/Shoot");
            _transformBack = GetNode<AudioStreamPlayer>("SFX/TransformBack");
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            HandleMove(delta);
            HandleShot();
            HandleInvincibility();
            HandleDevilTrigger();
            HandleAngelGraceDefense();
        }

        /// <summary>
        /// Takes the delta provided, and moves the player based on the provided delta
        /// </summary>
        /// <param name="delta"></param>
        private void HandleMove(float delta)
        {
            var velocity = new Vector2();

            var newAnimation = DEFAULT_ANIMATION_NAME;

            if (Input.IsActionPressed(RIGHT_BUTTON))
            {
                velocity.x += 1;
                newAnimation = RIGHT_ANIMATION_NAME;
            }

            if (Input.IsActionPressed(LEFT_BUTTON))
            {
                velocity.x -= 1;
                newAnimation = LEFT_ANIMATION_NAME;
            }

            if (Input.IsActionPressed(DOWN_BUTTON))
            {
                velocity.y += 1;
            }

            if (Input.IsActionPressed(UP_BUTTON))
            {
                velocity.y -= 1;
            }

            ((AnimationPlayer)GetNode("AnimationPlayer")).Play(newAnimation);

            // If any direction was pressed
            if (velocity.Length() > 0)
            {
                var speedToUse = PlayerSpeed;
                // Change speed if the player is eligible for slow down
                if (_currentTimeBeforeSlow >= TimeBeforeSlow)
                    speedToUse = PlayerSpeedSlow;
                velocity = velocity.Normalized() * speedToUse;
            }

            var newPosition = Position + velocity * delta;

            // Lock the player within the screen space, and bottom bar allowance
            Position = new Vector2(Mathf.Clamp(newPosition.x, 0, _screenSize.x),
                Mathf.Clamp(newPosition.y, 0, _screenSize.y - BottomBarAllowance));
        }

        /// <summary>
        /// Handles all shot logic for the player
        /// </summary>
        private void HandleShot()
        {
            _currentFramesBeforeNextShot--;
            var lastShot = string.Empty;
            if (Input.IsActionPressed(ANGEL_GRACE_SHOOT_BUTTON) || Input.IsActionPressed(HELLFIRE_SHOOT_BUTTON))
            {
                // Players will slow down if any shot button is held down
                if (_currentTimeBeforeSlow < TimeBeforeSlow)
                    _currentTimeBeforeSlow++;

                // Reset shot duration
                _currentShotDuration = 0;

                // Set last shot
                if(Input.IsActionPressed(ANGEL_GRACE_SHOOT_BUTTON))
                {
                    lastShot = ANGEL_GRACE_SHOOT_BUTTON;
                }
                else
                {
                    lastShot = HELLFIRE_SHOOT_BUTTON;
                }

                TryBombShot(lastShot);
            }
            else
            {
                _currentTimeBeforeSlow = 0;
            }

            // Only shoot if another shot is allowed by frames before shot, and shot duration
            if (_currentFramesBeforeNextShot <= 0 && _currentShotDuration <= ShotDuration)
            {
                _currentFramesBeforeNextShot = FramesBeforeNextShot;
                _currentShotDuration++;
                var shotNodes = GetNode("ShotStartPosition/Level" + _globalPlayerStats.HellfireLevel).GetChildren(); // Get the type of shot nodes based on the Hellfire level
                foreach(var shotPosition in shotNodes)
                {
                    SpawnBullet(shotPosition, lastShot);
                }
            }
        }

        /// <summary>
        /// Spawns bullet of the lastShot type, in the shotPosition provided
        /// </summary>
        /// <param name="shotPosition">This is an object since it is a Node from the Player's Scene entity</param>
        /// <param name="lastShot"></param>
        private void SpawnBullet(object shotPosition, string lastShot)
        {
            Node bullet;
            var devilTriggerShot = false;

            _shoot.Play(0);

            // Determine shot based on whether the player is in Devil Trigger, or based on button pressed
            if (_currentDevilTriggerTime > 0)
            {
                bullet = IridescentShot.Instance();
                devilTriggerShot = true;
            }
            else if (lastShot.Equals(ANGEL_GRACE_SHOOT_BUTTON))
            {
                bullet = AngelGraceShot.Instance();
            }
            else
            {
                bullet = HellfireShot.Instance();
            }
            var bulletPosition = Position + ((Position2D)shotPosition).Position;
            ((Area2D)bullet).Position = bulletPosition;
            GetParent().AddChild(bullet);
            ((PlayerBullet)bullet).SetShotStrength(devilTriggerShot ? DevilTriggerBulletStrength : ShotStrength); // Set shot strength based on devil trigger mode
        }

        /// <summary>
        /// Determines what bomb to shoot based on the last shot
        /// </summary>
        /// <param name="lastShot"></param>
        private void TryBombShot(string lastShot)
        {
            if (Input.IsActionJustPressed(SPECIAL_ACTION_BUTTON))
            {
                Node bomb;

                var bombPosition = new Vector2(_screenSize.x / 2, _screenSize.y / 2); // Bomb will always be in the middle of the screen 

                // Angel and Hellfire bombs act differently
                if (_globalPlayerStats.AngelGraceLevel > 0 && lastShot.Equals(ANGEL_GRACE_SHOOT_BUTTON))
                {
                    // AngelBomb does something else
                    bomb = AngelBomb.Instance();
                    _globalPlayerStats.DecreaseAngelGraceLevel(1);
                    ((Area2D)bomb).Position = bombPosition;
                    _bomb.Play(0);
                    GetParent().AddChild(bomb);
                }
                else if(_globalPlayerStats.HellfireLevel > 0 && lastShot.Equals(HELLFIRE_SHOOT_BUTTON))
                {
                    // Hellfire does damage
                    bomb = HellfireBomb.Instance();
                    _globalPlayerStats.DecreaseHellfireLevel(1);
                    ((PlayerDemonBomb)bomb).SetShotStrength(HellfireBombStrength);
                    ((Area2D)bomb).Position = bombPosition;
                    _bomb.Play(0);
                    GetParent().AddChild(bomb);
                }
            }
        }

        /// <summary>
        /// Sets the invincibility for the user
        /// </summary>
        private void HandleInvincibility()
        {
            if (_currentInvincibilityFrames > 0)
            {
                _currentInvincibilityFrames--;
                CallDeferred("ShowInvincibilityChanges");
            }
            else
                CallDeferred("set_invincibility", false); // Allows for asynchronously setting invincibility
        }

        /// <summary>
        /// Handles setting the player's devil trigger status
        /// </summary>
        private void HandleDevilTrigger()
        {
            if (_currentDevilTriggerTime > 0)
            {
                _currentDevilTriggerTime--;
                _devilTriggerBar.Value = _currentDevilTriggerTime;
            }
            else
            {

                // If the devil trigger time is 0, set the sprite and UI back to normal
                if (_devilTriggerBar.Visible)
                {
                    _transformBack.Play(0);
                    _sprite.Texture = NormalSprite;
                    _devilTriggerBar.Visible = false;
                }

                // If ONLY the special action buttton is pressed, and when there is both Hellfire and AngelGrace levels to spare, go into devil trigger
                if (Input.IsActionPressed(SPECIAL_ACTION_BUTTON) &&
                    !(Input.IsActionPressed(ANGEL_GRACE_SHOOT_BUTTON) || Input.IsActionPressed(HELLFIRE_SHOOT_BUTTON)) &&
                    (_globalPlayerStats.HellfireLevel > 0 && _globalPlayerStats.AngelGraceLevel > 0))
                {
                    ChangeToDevilTrigger(_sprite);
                }
            }

        }

        /// <summary>
        /// Morphs player into Devil Trigger mode, and modifies the provided spriteNode
        /// </summary>
        /// <param name="spriteNode"></param>
        private void ChangeToDevilTrigger(Sprite spriteNode)
        {
            // Decrease all levels
            _globalPlayerStats.DecreaseAngelGraceLevel(1);
            _globalPlayerStats.DecreaseHellfireLevel(1);

            // Set devil trigger time
            _currentDevilTriggerTime = DevilTriggerTime;

            // Go invincible
            CallDeferred("set_invincibility", true);

            // Do SFX
            _transform.Play(0);

            // Change the sprite and UI
            spriteNode.Texture = PowerUpSprite;
            var powerUpSprite = (AnimatedSprite)GetNode("PowerUpSprite");
            powerUpSprite.Play("power_up");
            _devilTriggerBar.Visible = true;
            _devilTriggerBar.Value = DevilTriggerTime;
        }

        /// <summary>
        /// Handles the shield size based on the AngleGracel level. 
        /// </summary>
        private void HandleAngelGraceDefense()
        {
            var shieldBox = (CollisionShape2D)GetNode("ShieldBox/ShieldBox");

            // Using "SetScale()" method on sheildBox's nodes does not affect it, so we have to assign a new scale instead
            if (_globalPlayerStats.AngelGraceLevel >= 3)
            {
                shieldBox.Scale = new Vector2(_globalPlayerStats.AngelGraceHitboxSizeLevel3, _globalPlayerStats.AngelGraceHitboxSizeLevel3);
            }
            else if (_globalPlayerStats.AngelGraceLevel == 2)
            {
                shieldBox.Scale = new Vector2(_globalPlayerStats.AngelGraceHitboxSizeLevel2, _globalPlayerStats.AngelGraceHitboxSizeLevel2);
            }
            else
            {
                shieldBox.Scale = new Vector2(_globalPlayerStats.AngelGraceHitboxSizeLevel1, _globalPlayerStats.AngelGraceHitboxSizeLevel1);
            }
        }

        /// <summary>
        /// Handles event when the player's area is entered by another Area2D, which is usually an enemy or enemy bullet
        /// </summary>
        /// <param name="area"></param>
        public void _on_Player_area_entered(Area2D area)
        {
            HandleHit(area);
        }

        /// <summary>
        /// Handles how the player is affected when hit by an enemy 
        /// </summary>
        /// <param name="area"></param>
        private void HandleHit(Area2D area)
        {
            // Only affected if not invincible, and the are is ENEMY or ENEMY BULLET that is AIRBORNE
            if (_currentInvincibilityFrames <= 0 && (area.IsInGroup(GroupConstants.ENEMY_BULLET_GROUP) || area.IsInGroup(GroupConstants.ENEMY_GROUP)) &&
                (
                area is EnemyAttributesBase && ((EnemyAttributesBase)area).IsAirborne
                || 
                area is EnemyBulletBase && ((EnemyBulletBase)area).IsAirborne
                ))
            {
                area.QueueFree();
                _globalPlayerStats.DecreaseLives(1);

                _death.Play(0);

                // Remove player when lives reach 0
                if (_globalPlayerStats.Lives <= 0)
                {
                    QueueFree();
                }
                else
                {
                    CallDeferred("set_invincibility", true); // Set invinciblity asynchronously 
                }
            }
        }

        /// <summary>
        /// Handles player display as they become less invincible
        /// </summary>
        private void ShowInvincibilityChanges()
        {
            bool shouldFlash;
            if (_currentInvincibilityFrames > 50)
                shouldFlash = _currentInvincibilityFrames % 8 == 0;
            else if (_currentInvincibilityFrames > 30)
                shouldFlash = _currentInvincibilityFrames % 5 == 0;
            else if (_currentInvincibilityFrames > 5)
                shouldFlash = _currentInvincibilityFrames % 3 == 0;
            else 
                shouldFlash = true;

            var nextValue = _sprite.Modulate.a == 0 ? 1 : 0;

            if (shouldFlash)
                _sprite.Modulate = new Color(1, 1, 1, nextValue);
        }

        /// <summary>
        /// Sets the invincibility based on the invincible parameter 
        /// </summary>
        /// <param name="invincible"></param>
        public void set_invincibility(bool invincible)
        {
            if (invincible)
            {
                _currentInvincibilityFrames = InvincibilityTimeFrame; // Start invincibility time
                _sprite.Modulate = new Color(1, 1, 1, 0.5f); // Set transparency
            }
            else
            {
                _sprite.Modulate = new Color(1, 1, 1, 1f); // Remove transparency
            }
        }

        /// <summary>
        /// End stage event that is called externally. This makes the player invincible
        /// </summary>
        public void end_stage()
        {
            CallDeferred("set_invincibility", true);
        }
    }
}



