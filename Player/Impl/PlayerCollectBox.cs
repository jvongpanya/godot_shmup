using Godot;
using Shmupper.Items.Impl;
using Shmupper.Utilities;

namespace Shmupper.Player.Impl
{
    public class PlayerCollectBox : Area2D
    {
        private GlobalPlayerStats _globalPlayerStats;
        private AudioStreamPlayer _collect;

        public override void _Ready()
        {
            base._Ready();
            _globalPlayerStats = StageConstants.GetGlobalPlayerStatsNode(this);
            _collect = GetNode<AudioStreamPlayer>("Collect");
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
        }

        /// <summary>
        /// Event that fires when the collectbox is hit
        /// </summary>
        /// <param name="body"></param>
        private void _on_CollectBox_body_entered(object body)
        {
            HandleAngelGraceItem((PhysicsBody2D)body);
            HandleHellfireItem((PhysicsBody2D)body);
            HandleCollectibleItem((PhysicsBody2D)body);
        }

        /// <summary>
        /// Modifies AngelGrace and score
        /// </summary>
        /// <param name="body"></param>
        private void HandleAngelGraceItem(PhysicsBody2D body)
        {
            if (body.IsInGroup(GroupConstants.ANGEL_GRACE_ITEM_GROUP))
            {
                var collectible = body as CollectibleBase;
                _globalPlayerStats.IncreaseAngelGrace(collectible);
                _globalPlayerStats.AddToScore(collectible);
                _collect.Play();
                body.QueueFree();
            }
        }

        /// <summary>
        /// Modifies Hellfire and score
        /// </summary>
        /// <param name="body"></param>
        private void HandleHellfireItem(PhysicsBody2D body)
        {
            if (body.IsInGroup(GroupConstants.HELLFIRE_ITEM_GROUP))
            {
                var collectible = body as CollectibleBase;
                _globalPlayerStats.IncreaseHellfire(collectible);
                _globalPlayerStats.AddToScore(collectible);
                _collect.Play();
                body.QueueFree();
            }
        }

        /// <summary>
        /// Modifies score
        /// </summary>
        /// <param name="body"></param>
        private void HandleCollectibleItem(PhysicsBody2D body)
        {
            if (body.IsInGroup(GroupConstants.COLLECTIBLE_ITEM_GROUP))
            {
                var collectible = body as CollectibleBase;
                _globalPlayerStats.AddToScore(collectible);
                _collect.Play();
                body.QueueFree();
            }
        }

    }
}
