﻿using Shmupper.SharedInterfaces;

namespace Shmupper.Player.Impl
{
    /// <summary>
    /// Base implementation of objects that represent "levels"
    /// </summary>
    public abstract class LevelingBase : IHasLeveling
    {
        public int CurrentLevel { get; protected set; }

        public int CurrentExperience { get; protected set; }

        public int CurrentExperienceThreshold { get; protected set; }

        public int MaxLevel { get; }

        protected bool BelowMaxLevel => CurrentLevel < MaxLevel;

        protected LevelingBase(int maxLevel, int defaultLevel, int threshold)
        {
            MaxLevel = maxLevel;
            CurrentLevel = defaultLevel;
            CurrentExperienceThreshold = threshold;
        }

        public void DecreaseLevel(int amount)
        {
            CurrentLevel -= amount;
        }

        /// <summary>
        /// Adds the amount provided to the CurrentExperience. 
        /// Returns whether a level up occurred.
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public bool IncreaseExperienceWithThreshold(int amount)
        {
            if (amount > 0 && BelowMaxLevel)
            {
                var newValue = CurrentExperience + amount;

                // If the new value does not reach the threshold, and the level is not maxed, just set the CurrentExperience normally
                if (newValue < CurrentExperienceThreshold)
                {
                    CurrentExperience = newValue;
                    return false;
                }
                else
                {
                    CurrentExperience = newValue - CurrentExperienceThreshold;
                    CurrentLevel += 1;

                    // Reset experience if max level is reached
                    if (!BelowMaxLevel)
                        CurrentExperience = 0;
                }
            }
            return false;
        }
    }
}
