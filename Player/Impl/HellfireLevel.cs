﻿namespace Shmupper.Player.Impl
{
    /// <summary>
    /// Represents the "AngelGrace" level
    /// </summary>
    public class HellfireLevel : LevelingBase
    {
        public HellfireLevel(int maxLevel, int defaultLevel, int threshold) : base(maxLevel, defaultLevel, threshold)
        {
        }
    }
}
