﻿using Godot;
using Shmupper.SharedInterfaces;
using Shmupper.Utilities;

namespace Shmupper.Player.Impl
{
    /// <summary>
    /// This class is designated as a Singleton in the Godot project. 
    /// It is currently not a traditional singleton due to it currently being used by remaining GDScripts. 
    /// Once GDScripts are all converted into C#, this can be converted into a true singleton
    /// </summary>
    public class GlobalPlayerStats : Node
    {
        // Lives
        public int Lives { get; set; }

        // Score
        public int CurrentScore { get; private set; }

        /// <summary>
        /// All properties related to AngelGrace, including leveling and properties
        /// </summary>
        #region AngleGrace
        public int AngelGraceLevelThreshold => 50;
        public int AngelGraceMaxLevel => 3;
        public float AngelGraceHitboxSizeLevel1 => 1f;
        public float AngelGraceHitboxSizeLevel2 => 2f;
        public float AngelGraceHitboxSizeLevel3 => 3f;
        public int AngelGraceLevel => AngelGraceLevelContainer.CurrentLevel;
        public int AngelGrace => AngelGraceLevelContainer.CurrentExperience;
        private IHasLeveling AngelGraceLevelContainer { get; set; }
        #endregion

        /// <summary>
        /// All properties related to Hellfire, including leveling and properties
        /// </summary>
        #region Hellfire
        public int HellfireLevelThreshold => 50;
        public int HellfireMaxLevel => 3;
        public int HellfireLevel => HellfireLevelContainer.CurrentLevel;
        public int Hellfire => HellfireLevelContainer.CurrentExperience;
        private IHasLeveling HellfireLevelContainer { get; set; }
        #endregion

        private Vector2 _defaultPlayerVector;

        public override void _Ready()
        {
            ResetModifiableStats();
            _defaultPlayerVector = new Vector2(72, 200); // Points downward
        }

        public void ResetModifiableStats()
        {
            // AngelGrace and Hellfire default to level 1
            AngelGraceLevelContainer = new AngelGraceLevel(AngelGraceMaxLevel, 1, AngelGraceLevelThreshold);
            HellfireLevelContainer = new HellfireLevel(HellfireMaxLevel, 1, HellfireLevelThreshold);
            CurrentScore = 0;
            Lives = 3;
        }

        /// <summary>
        /// Returns a normalized vector pointing from the caller to the player
        /// </summary>
        /// <param name="caller"></param>
        /// <returns>Vector2 to the direction of the player</returns>
        public Vector2 GetDirectionToPlayer(Node2D caller)
        {
            var playerLocation = GetPlayerLocation();
            var directionToPlayer = playerLocation - caller.GlobalPosition;
            directionToPlayer = directionToPlayer.Normalized();
            return directionToPlayer; 
        }

        /// <summary>
        /// Returns a vector representing the player's location
        /// </summary>
        /// <returns>Vector2 of the player's location</returns>
        public Vector2 GetPlayerLocation()
        {
            var playerNodes = GetParent().GetTree().GetNodesInGroup(GroupConstants.PLAYER_GROUP);
            if (playerNodes.Count > 0 && playerNodes[0] != null)
                return ((Node2D)playerNodes[0]).GlobalPosition;
            return _defaultPlayerVector; 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="score"></param>
        public void AddToScore(IHasScore score)
        {
            CurrentScore += score.InternalScore; 
        }

        /// <summary>
        /// Decreases Lives by the provided lives amount.
        /// </summary>
        /// <param name="lives"></param>
        public void DecreaseLives(int lives)
        {
            Lives -= lives; 
        }

        /// <summary>
        /// Decreases AngleGrace level by the decreaseAmount
        /// </summary>
        /// <param name="decreaseAmount"></param>
        public void DecreaseAngelGraceLevel(int decreaseAmount)
        {
            AngelGraceLevelContainer.DecreaseLevel(decreaseAmount);
        }

        /// <summary>
        /// Decreases Hellfire level by the decreaseAmount
        /// </summary>
        /// <param name="decreaseAmount"></param>
        public void DecreaseHellfireLevel(int decreaseAmount)
        {
            HellfireLevelContainer.DecreaseLevel(decreaseAmount);
        }

        /// <summary>
        /// Increases the AngelGrace experience by provided amount
        /// </summary>
        /// <param name="addedAmount"></param>
        public void IncreaseAngelGrace(IHasExperience addedAmount)
        {
            AngelGraceLevelContainer.IncreaseExperienceWithThreshold(addedAmount.InternalExperience);
        }

        /// <summary>
        /// Increases the Hellfire experience by provided amount
        /// </summary>
        /// <param name="addedAmount"></param>
        public void IncreaseHellfire(IHasExperience addedAmount)
        {
            HellfireLevelContainer.IncreaseExperienceWithThreshold(addedAmount.InternalExperience);
        }
    }
}
