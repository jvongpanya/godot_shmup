﻿namespace Shmupper.Player.Impl
{
    /// <summary>
    /// Represents the "AngelGrace" level
    /// </summary>
    public class AngelGraceLevel : LevelingBase
    {
        public AngelGraceLevel(int maxLevel, int defaultLevel, int threshold) : base(maxLevel, defaultLevel, threshold)
        {
        }
    }
}
