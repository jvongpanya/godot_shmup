﻿using Shmupper.Bullet.Impl;

namespace Shmupper.Bullet.Interface
{
    public interface IEnemyBulletShooter
    {
        bool StepShoot(EnemyShotProperties enemyShotProperties);
        void ResetShotParameters();
    }
}
