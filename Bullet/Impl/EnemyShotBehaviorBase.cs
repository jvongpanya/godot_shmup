using Godot;
using Shmupper.Player.Impl;
using Shmupper.Utilities;

namespace Shmupper.Bullet.Impl
{
    /// <summary>
    /// The base class for shot behavior. 
    /// This is a Position2D to represent that it just a position within the scene of the Godot engine.
    /// </summary>
    public abstract class EnemyShotBehaviorBase : Position2D
    {
        protected GlobalPlayerStats GlobalPlayerStats;

        public override void _Ready()
        {
            base._Ready();
            GlobalPlayerStats = StageConstants.GetGlobalPlayerStatsNode(this);
        }
    }
}
