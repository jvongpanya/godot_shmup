﻿using Godot;
using Shmupper.Player.Impl;

namespace Shmupper.Bullet.Impl
{
    /// <summary>
    /// Shoots bullets in a spiral pattern around the parent
    /// </summary>
    public class SpiralStepShooter : EnemyBulletShooterBase
    {
        public int NumberOfSpirals { get; }

        /// <summary>
        /// The angle between the shots if more than one bullet is shot from this spiral
        /// </summary>
        public int PairDegreeInterval { get; }

        /// <summary>
        /// The rate at which the degree changes between bullet shots
        /// </summary>
        public int MainRateDegreeInterval { get; }

        /// <summary>
        /// Determines whether an opposite shot is fired for each shot fired
        /// </summary>
        public bool MirrorShot { get; }

        public SpiralStepShooter(float maxShotDelay, int maxShots, GlobalPlayerStats globalPlayerStats, Position2D callingPosition
            ,int numberOfSpirals
            ,int pairDegreeInterval
            ,int mainRateDegreeInterval
            ,bool mirrorShot) : base(maxShotDelay, maxShots, globalPlayerStats, callingPosition)
        {
            NumberOfSpirals = numberOfSpirals;
            PairDegreeInterval = pairDegreeInterval;
            MainRateDegreeInterval = mainRateDegreeInterval;
            MirrorShot = mirrorShot;
        }

        protected override void StepShoot_Internal(EnemyShotProperties enemyShotProperties)
        {
            for (int interval = 1; interval < NumberOfSpirals + 1; interval++)
            {
                var rate = Mathf.Deg2Rad((interval * PairDegreeInterval) + (CurrentShots * MainRateDegreeInterval)); // The direction of the vector is determined by the current shot (which position to start), and the degree variables
                var mainSpiralVector = new Vector2(Mathf.Cos(rate), Mathf.Sin(rate));
                mainSpiralVector = mainSpiralVector.Normalized();
                enemyShotProperties.Vector = mainSpiralVector;
                CreateBullet(enemyShotProperties);

                // Creates an opposite shot if MirrorShot 
                if (MirrorShot)
                {
                    var oppositeSpiralVector = new Vector2(-Mathf.Cos(rate), -Mathf.Sin(rate));
                    oppositeSpiralVector = oppositeSpiralVector.Normalized();
                    enemyShotProperties.Vector = oppositeSpiralVector;
                    CreateBullet(enemyShotProperties);
                }
            }
        }
    }
}
