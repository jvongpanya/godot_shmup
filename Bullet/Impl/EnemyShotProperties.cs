﻿using Godot;

namespace Shmupper.Bullet.Impl
{
    /// <summary>
    /// The basic properties of an enemy shot. 
    /// This is a Node, which is the most basic object in Godot.
    /// TODO: Being a node is a remnant of transferring from GDScript. Look into removing this relationship if it is not needed.
    /// </summary>
    public class EnemyShotProperties : Node
    {
        public Vector2 Vector { get; set; }
        public float BulletSpeed { get; set; }
        public bool DoTween { get; set; }
        public float TweenTransitionTime  { get; set; }
        public bool TweenRepeat { get; set; }
        public PackedScene ShotAsset { get; set; }
    }
}
