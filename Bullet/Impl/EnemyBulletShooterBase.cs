﻿using Godot;
using Shmupper.Bullet.Interface;
using Shmupper.Enemy;
using Shmupper.Player.Impl;

namespace Shmupper.Bullet.Impl
{
    /// <summary>
    /// Represents the location and behavior of how a bullet will be shot by an enemy
    /// </summary>
    public abstract class EnemyBulletShooterBase : IEnemyBulletShooter
    {
        protected GlobalPlayerStats GlobalPlayerStats { get; }
        protected Position2D CallingPosition { get; }

        /// <summary>
        /// The number of shots this bullet shooter can perform
        /// </summary>
        protected int MaxShots { get; }

        protected bool ShootInfinitely { get; }

        /// <summary>
        /// The number of frames to wait before shooting the next bullet
        /// </summary>
        protected float MaxShotDelay { get; }

        /// <summary>
        /// The current number of frames waited on
        /// </summary>
        protected float CurrentShotDelay;

        /// <summary>
        /// Current shots fired
        /// </summary>
        protected float CurrentShots;

        protected EnemyBulletShooterBase(float maxShotDelay, 
            int maxShots, 
            GlobalPlayerStats globalPlayerStats, 
            Position2D callingPosition, 
            bool shootInfinitely = false)
        {
            MaxShotDelay = maxShotDelay;
            MaxShots = maxShots;
            GlobalPlayerStats = globalPlayerStats;
            CallingPosition = callingPosition;
            ShootInfinitely = shootInfinitely;
        }

        /// <summary>
        /// Uses the enemyShotProperties to create a shot based on the implementation. Returns whether the max number of shots have been released.
        /// A "step" is counted as each time the maximum delay is reached.
        /// </summary>
        /// <param name="enemyShotProperties"></param>
        /// <returns>Whether the max shots have been released</returns>
        public bool StepShoot(EnemyShotProperties enemyShotProperties)
        {
            if (CurrentShotDelay >= MaxShotDelay)
            {
                if (ShootInfinitely || (CurrentShots < MaxShots))
                {
                    CurrentShotDelay = 0; // Shot delay reset so they can start over
                    StepShoot_Internal(enemyShotProperties);
                    CurrentShots++;
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                CurrentShotDelay++; // If the max delay isn't reached, keep adding to the delay
                return false;
            }
        }

        protected abstract void StepShoot_Internal(EnemyShotProperties enemyShotProperties);

        public void ResetShotParameters()
        {
            CurrentShotDelay = 0;
            CurrentShots = 0;
        }

        /// <summary>
        /// Obtains the angle to the player from this shooter's position
        /// </summary>
        /// <returns></returns>
        protected float GetAngleToPlayer()
        {
            var playerLocation = GlobalPlayerStats.GetPlayerLocation();
            var angleToPlayer = CallingPosition.GetAngleTo(playerLocation);
            return angleToPlayer;
        }

        /// <summary>
        /// Uses the shotProperties to create a new bullet, set it up, and put it on this shooter
        /// </summary>
        /// <param name="shotProperties"></param>
        protected void CreateBullet(EnemyShotProperties shotProperties)
        {
            var newBullet = (Area2D)shotProperties.ShotAsset.Instance();
            ((EnemyBulletBase)newBullet).SetBulletProperties(shotProperties);
            ((EnemyBulletBase)newBullet).ConnectoToRelatedDestroyed(CallingPosition.GetParent(), ((EnemyAttributesBase)CallingPosition.GetParent()).EnemyId); // Connects the bullet to this shooter's parent's destruction event by ID
            CallingPosition.GetParent().GetParent().GetParent().GetParent().AddChild(newBullet); // This puts the bullet in the main scene, so it does not get destroyed if the shooter or shooter's owner is destroyed.
            ((EnemyBulletBase)newBullet).Position = CallingPosition.GlobalPosition;
        }
    }
}
