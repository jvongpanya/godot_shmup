﻿using Godot;
using Shmupper.Player.Impl;

namespace Shmupper.Bullet.Impl
{
    /// <summary>
    /// Shoots the bullet in a sine wave pattern towards the player
    /// </summary>
    public class SineWaveStepShooter : EnemyBulletShooterBase
    {
        /// <summary>
        /// How large of a sine wave will be shot
        /// </summary>
        public float SineShotDegreeInterval { get; }

        /// <summary>
        /// Whether a second sine wave is shot. The second wave is opposite of the main one.
        /// </summary>
        public bool CrossingShots { get; }

        public SineWaveStepShooter(float maxShotDelay, int maxShots, GlobalPlayerStats globalPlayerStats, Position2D callingPosition
            ,float sineShotDegreeInterval
            ,bool crossingShots) : base(maxShotDelay, maxShots, globalPlayerStats, callingPosition)
        {
            SineShotDegreeInterval = sineShotDegreeInterval;
            CrossingShots = crossingShots;
        }

        protected override void StepShoot_Internal(EnemyShotProperties enemyShotProperties)
        {
            var rate = Mathf.Deg2Rad(CurrentShots * SineShotDegreeInterval);
            var angleToPlayer = GetAngleToPlayer();
            var bulletVector = new Vector2(1, Mathf.Sin(rate)).Rotated(angleToPlayer);
            bulletVector = bulletVector.Normalized();
            enemyShotProperties.Vector = bulletVector;
            CreateBullet(enemyShotProperties);

            // If CrossingShots, creates another bullet that travels in the opposite direction
            if (CrossingShots)
            {
                var bulletVector2 = new Vector2(1, -Mathf.Sin(rate)).Rotated(angleToPlayer);
                bulletVector2 = bulletVector2.Normalized();
                enemyShotProperties.Vector = bulletVector2;
                CreateBullet(enemyShotProperties);
            }
        }
    }
}
