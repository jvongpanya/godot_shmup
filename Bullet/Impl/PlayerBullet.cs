using Godot;
using Shmupper.Player.Impl;
using Shmupper.SharedInterfaces;
using Shmupper.Utilities;

namespace Shmupper.scenes.bullets.Impl
{
    /// <summary>
    /// Represents the player's bullet and its behavior
    /// This is an Area2D to represent that it takes space within a scene in the Godot engine
    /// </summary>
    public class PlayerBullet : Area2D, IHasStrength, IHasScore
    {
        [Export]
        public float ShotSpeed = 5;

        [Export]
        public int ShotScore = 50;

        /// <summary>
        /// This is just the ShotScore. 
        /// This is set in a different property to standardize the score within the code, since ShotScore was declared differently on some Godot objects in the original GDScript implementation.
        /// </summary>
        public int InternalScore 
        {
            get { return ShotScore;  }
            set { ShotScore = value; }
        }
		
        /// <summary>
        /// Represents when the enemy is hit with the correct bullet
        /// </summary>
		[Export]
		public PackedScene HitSpark;

        /// <summary>
        /// Represents when the enemy is hit with the incorrect bullet
        /// </summary>
        [Export]
        public PackedScene BadHitSpark;

        public float ShotStrength { get; set; }

        private Vector2 _bulletVector;

        private GlobalPlayerStats _globalPlayerStats;

        public override void _Ready()
        {
            base._Ready();
            _bulletVector = new Vector2(0, -ShotSpeed); // Defaults bullet to shoot straight upwards
            GetNode("VisibilityNotifier2D").Connect("screen_exited", this, "_on_screen_exited");
            _globalPlayerStats = StageConstants.GetGlobalPlayerStatsNode(this);
        }

        public override void _Process(float delta)
        {
            base._Process(delta);

            // TODO: This is a remnant of switching animation types mid-project. The AnimationPlayer may not be provided on all player bullet types
            if(HasNode("AnimationPlayer"))
                ((AnimationPlayer)GetNode("AnimationPlayer")).Play("firing");

            // Each frame, move position up one bullet vector
            Position += _bulletVector;
        }

        public void SetShotStrength(float strength)
        {
            ShotStrength = strength;
        }

        public void _on_screen_exited()
        {
            QueueFree();
        }

        /// <summary>
        /// Interacts with the enemy if the entity that enters this bullet is an enemy.
        /// Determines the hitspark used, affects scoring, and destroys the bullet. 
        /// </summary>
        /// <param name="area"></param>
        public void _on_PlayerBullet_area_entered(Area2D area)
        {
            if (area.IsInGroup(GroupConstants.ENEMY_GROUP))
            {
                _globalPlayerStats.AddToScore(this);
                CreateHitSpark(area);
                QueueFree();
            }
        }

        private void CreateHitSpark(Area2D area)
        {
            Node hitspark;

            // Determines which hitspark to use depending on the enemy type, and the type of this bullet
            if ((area.IsInGroup(GroupConstants.ENEMY_ANGEL_GROUP) && IsInGroup(GroupConstants.PLAYER_ANGEL_BULLET_GROUP)) ||
                (area.IsInGroup(GroupConstants.ENEMY_DEMON_GROUP) && IsInGroup(GroupConstants.PLAYER_DEMON_BULLET_GROUP)))
            {
                hitspark = HitSpark.Instance();
            }
            else
            {
                hitspark = BadHitSpark.Instance();
            }
            GetParent().AddChild(hitspark);
            ((Area2D)hitspark).Position = Position;
        }

    }
}
