﻿using Godot;
using Shmupper.Bullet.Impl;
using Shmupper.Player.Impl;

namespace Shmupper.Bullet.Impl
{
    /// <summary>
    /// Shoots the bullet towards the player
    /// </summary>
    public class HomingBulletStepShooter : EnemyBulletShooterBase
    {
        public HomingBulletStepShooter(float maxShotDelay, 
            int maxShots, 
            GlobalPlayerStats globalPlayerStats, 
            Position2D callingPosition) : base(maxShotDelay, maxShots, globalPlayerStats, callingPosition)
        {
            
        }

        protected override void StepShoot_Internal(EnemyShotProperties enemyShotProperties)
        {
            var bulletVector = GlobalPlayerStats.GetDirectionToPlayer(CallingPosition);
            bulletVector = bulletVector.Normalized(); // Must be normalized so only direction is provided by the vector.
            enemyShotProperties.Vector = bulletVector;
            CreateBullet(enemyShotProperties);
        }
    }
}
