﻿using Godot;
using Shmupper.Player.Impl;

namespace Shmupper.Bullet.Impl
{
    /// <summary>
    /// Shoots bullets in a spread pattern from the parent towards the player.
    /// </summary>
    public class SpreadShotStepShooter : EnemyBulletShooterBase
    {
        /// <summary>
        /// The degrees between each bullet shot
        /// </summary>
        public float BulletDegreeInterval { get; set; }

        /// <summary>
        /// The number of bullets to shoot at a time from the spread
        /// </summary>
        public int ConcurrentBullets { get; set; }

        public SpreadShotStepShooter(float maxShotDelay, int maxShots, GlobalPlayerStats globalPlayerStats, Position2D callingPosition
            ,float bulletDegreeInterval
            ,int concurrentBullets) : base(maxShotDelay, maxShots, globalPlayerStats, callingPosition)
        {
            BulletDegreeInterval = bulletDegreeInterval;
            ConcurrentBullets = concurrentBullets;
        }

        protected override void StepShoot_Internal(EnemyShotProperties enemyShotProperties)
        {
            var numberShootFloor = Mathf.Floor(ConcurrentBullets / 2); // Ensures that the correct number of shots are performed
            var noRemainder = ConcurrentBullets % 2 == 0;
            var topNumberShots = numberShootFloor;
            if (!noRemainder) // In case of an odd number, we round up to get back to the intended number of shots
                topNumberShots += 1;

            /* Starts at negative ConcurrentBullets since the vector needs to start at the opposite side of the shooter's position. 
             * This way, the player is centered within the spread
             */
            for (int individualBullets = -ConcurrentBullets; individualBullets < topNumberShots; individualBullets++)
            {
                var bulletVector = GlobalPlayerStats.GetDirectionToPlayer(CallingPosition);
                bulletVector = bulletVector.Rotated(Mathf.Deg2Rad(BulletDegreeInterval * individualBullets));
                bulletVector = bulletVector.Normalized();
                enemyShotProperties.Vector = bulletVector;
                CreateBullet(enemyShotProperties);
            }
        }
    }
}
