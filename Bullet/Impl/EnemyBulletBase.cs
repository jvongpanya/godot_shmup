using Godot;
using Shmupper.Player.Impl;
using Shmupper.Utilities;

namespace Shmupper.Bullet.Impl
{
    /// <summary>
    /// The base representation of a bullet that sets various properties for how a bullet should act. 
    /// This is an Area2D to represent that it an object with some kind of volume in the scene within the Godot engine.
    /// </summary>
    public class EnemyBulletBase : Area2D
    {
        [Export]
        public bool IsAirborne = true;

        [Export]
        public float DefaultBulletSpeed = 1.5f;

        private Tween _tween;

        [Export]
        public PackedScene ItemDrop;

        // The object that is related to this bullet
        private string _relatedObjectId;

        private Vector2 _bulletVector;

        #region Bullet slowdown
        private float _originalBulletSpeed = 0f;
        private bool _alreadySlowed = false;
        #endregion

        public override void _Ready()
        {
            base._Ready();
            GetNode<VisibilityNotifier2D>("VisibilityNotifier2D").Connect("screen_exited", this, "_on_screen_exited");
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            ShootToVector(_bulletVector);
        }

        /// <summary>
        /// Shootes this bullet based on the provided vector
        /// </summary>
        /// <param name="vector"></param>
        private void ShootToVector(Vector2 vector)
        {
            Rotation = vector.Angle();
            Position += vector * DefaultBulletSpeed;
        }

        /// <summary>
        /// Updates this bullet's prpoerties with the provided shotProperties.
        /// </summary>
        /// <param name="shotProperties">Provides the vector, bullet speed, and tween behavior</param>
        public void SetBulletProperties(EnemyShotProperties shotProperties)
        {
            _bulletVector = shotProperties.Vector;
            DefaultBulletSpeed = shotProperties.BulletSpeed;
            _originalBulletSpeed = shotProperties.BulletSpeed;
            if (shotProperties.DoTween)
            {
                _tween = new Tween();
                AddChild(_tween);
                _tween.InterpolateProperty(this,
                    "DefaultBulletSpeed", // Tweens the DefaultBulletSpeed property on this object
                    0, DefaultBulletSpeed, shotProperties.TweenTransitionTime,
                    Tween.TransitionType.Linear,
                    Tween.EaseType.In,
                    0);
                _tween.Repeat = false;
                _tween.Start();
            }
        }

        /// <summary>
        /// Connects this bullet to the event, "enemy_destroyed_special", on the parent object, to the method, _on_related_destroyed(), with the relatedId.
        /// Firing "enemy_destroyed_special" causes this bullet to be destroyed AND drop items.
        /// </summary>
        /// <param name="parent">The entity that shot this bullet</param>
        /// <param name="relatedId">The ID of the entity</param>
        public void ConnectoToRelatedDestroyed(Node parent, string relatedId)
        {
            parent.Connect("enemy_destroyed_special", this, "_on_related_destroyed");
            _relatedObjectId = relatedId;
        }

        /// <summary>
        /// Event that fires.
        /// Executes the drop_item() method if the relatedId provided is equal to the _relatedObjectId, which should happen when the "enemy_destroyed_special" is executed.
        /// This is because the enemy_destroyed_special event could be executed by an object with a different ID. 
        /// </summary>
        /// <param name="relatedId"></param>
        public void _on_related_destroyed(string relatedId)
        {
            if (_relatedObjectId.Equals(relatedId))
            {
                CallDeferred("drop_item");
                QueueFree();
            }
        }

        public void _on_screen_exited()
        {
            QueueFree();
        }

        /// <summary>
        /// Adds the item dropped to the scene where this bullet exists.
        /// </summary>
        public void drop_item()
        {
            var item = (Node2D)ItemDrop.Instance();
            GetParent().AddChild(item);
            item.Position = Position;
        }
        
        /// <summary>
        /// Removes this bullet if the Area2D entity is a player angel bomb.
        /// </summary>
        /// <param name="area">The Area2D entity entering this bullet.</param>
        public void _on_EnemyBullet_area_entered(Area2D area)
        {
            if (area.IsInGroup(GroupConstants.PLAYER_ANGEL_BOMB_GROUP))
            {
                QueueFree();
            }
        }

        /// <summary>
        /// If slow is true, slows the bullet down if it has not already been slowed. Otherwise, speeds it back up.
        /// </summary>
        /// <param name="slow"></param>
        public void SlowBullet(bool slow)
        {
            if(slow && !_alreadySlowed)
            {
                DefaultBulletSpeed = DefaultBulletSpeed / 2;
                _alreadySlowed = true;
            }

            if(!slow)
            {
                DefaultBulletSpeed = _originalBulletSpeed;
                _alreadySlowed = false;
            }
        }
    }
}
