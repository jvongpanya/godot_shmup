using Godot;
using Shmupper.Enemy;
using Shmupper.Player.Impl;
using Shmupper.SharedInterfaces;
using Shmupper.Utilities;

namespace Shmupper.Bullet.Impl
{
    /// <summary>
    /// Represents the "Demon Bomb" from the player
    /// </summary>
    public class PlayerDemonBomb : Area2D, IHasStrength, IHasScore
    {
        [Export]
        public int ShotScore = 200;

        /// <summary>
        /// This is just the ShotScore. 
        /// This is set in a different property to standardize the score within the code, since ShotScore was declared differently on some Godot objects in the original GDScript implementation.
        /// </summary>
        public int InternalScore
        {
            get { return ShotScore;  }
            set { ShotScore = value; }
        }

        public float ShotStrength { get; set; }

        private GlobalPlayerStats _globalPlayerStats;

        public PlayerDemonBomb() { }
        

        public override void _Ready()
        {
            base._Ready();
            _globalPlayerStats = StageConstants.GetGlobalPlayerStatsNode(this);
        }
        
        /// <summary>
        /// Destroys this entity as soon as the animation is complete
        /// </summary>
        public void _on_AnimatedSprite_animation_finished()
        {
            QueueFree();
        }

        public void SetShotStrength(float strength)
        {
            ShotStrength = strength;
        }

        /// <summary>
        /// Event that fires when something enters this bomb's area. 
        /// If the area is an enemy, manipulates the score, and disables enemies from dropping items.
        /// </summary>
        /// <param name="area"></param>
        public void _on_PlayerBullet_area_entered(Area2D area)
        {
            if (area.IsInGroup(GroupConstants.ENEMY_GROUP))
            {
                _globalPlayerStats.AddToScore(this);
                if (IsInGroup(GroupConstants.PLAYER_BOMB))
                {
                    ((EnemyAttributesBase)area).DropItem = false;
                }
            }
        }
    }
}

