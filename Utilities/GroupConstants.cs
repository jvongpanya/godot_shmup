﻿namespace Shmupper.Utilities
{
    public static class GroupConstants
    {
        // Enemy
        public static string ENEMY_ANGEL_GROUP = "angel";
        public static string ENEMY_DEMON_GROUP = "demon";
        public static string ENEMY_GROUP = "enemy";
        public static string ENEMY_BULLET_GROUP = "enemy_bullet";

        // Player
        public static string PLAYER_GROUP = "player";

        // Player Bullets
        public static string PLAYER_BOMB = "player_bomb";
        public static string PLAYER_ANGEL_BOMB_GROUP = "player_angel_bomb";
        public static string PLAYER_BULLET_GROUP = "player_bullet";
        public static string PLAYER_ANGEL_BULLET_GROUP = "player_angel_bullet";
        public static string PLAYER_DEMON_BULLET_GROUP = "player_demon_bullet";
        public static string PLAYER_SPECIAL_BULLET = "player_bullet_special";

        // Items
        public static string ANGEL_GRACE_ITEM_GROUP = "angel_grace_item";
        public static string HELLFIRE_ITEM_GROUP = "hellfire_item";
        public static string COLLECTIBLE_ITEM_GROUP = "diamond_item";

    }
}
