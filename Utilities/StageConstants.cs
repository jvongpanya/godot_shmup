﻿using Godot;
using Shmupper.Player.Impl;
using System;

namespace Shmupper.Utilities
{
    public static class StageConstants
    {
        /// <summary>
        /// The library and path to the JSON file that represents the stage template
        /// </summary>
        public static string STAGE_RESOURCE_TEMPLATE = "Shmupper.scenes.stages.stage_{0}.resources.stage_{0}.json";

        // Expected root node children
        public static string GLOBAL_PLAYER_STATS_NODE = "/root/GlobalPlayerStats";
        public static string STAGE_NODE = "/root/stage";
        public static string PLAYER_NODE = "/root/stage/Player";

        /// <summary>
        /// Takes in the node, and returns the GlobalPlayerStats singleton. 
        /// The singleton is created by Godot, which is why it needs to be obtained as a Node.
        /// The node is obtained this way so it is only obtained once per caller, instead of always being reloaded when needed. 
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static GlobalPlayerStats GetGlobalPlayerStatsNode(Node node)
        {
            var playerStatsNode = GetNode(node, GLOBAL_PLAYER_STATS_NODE);
            if(playerStatsNode == null)
                throw new ArgumentException($"Node does not have the player stats node at: { STAGE_NODE }");

            return (GlobalPlayerStats)node.GetNode(GLOBAL_PLAYER_STATS_NODE);
        }

        /// <summary>
        /// Obtains the StageNode at the STAGE_NODE path. A node is required as a reference to use the Godot GetNode() function.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static Node GetStageNode(Node node)
        {
            var stageNode = GetNode(node, STAGE_NODE);
            if(stageNode == null)
                throw new ArgumentException($"Node does not have the stage node at: { STAGE_NODE }");
            return stageNode;
        }

        /// <summary>
        /// Obtains the PlayerNode at the PLAYER_NODE path. A node is required as a reference to use the Godot GetNode() function.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static Node GetPlayerNode(Node node)
        {
            return GetNode(node, PLAYER_NODE);
        }

        /// <summary>
        /// Uses the node to obtain the Node from the given nodePath, or null if the node is not found.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="nodePath"></param>
        /// <returns></returns>
        private static Node GetNode(Node node, string nodePath)
        {
            if (node.HasNode(nodePath))
                return node.GetNode(nodePath);
            return null;
        }
    }
}
